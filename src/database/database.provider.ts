import { Sequelize } from 'sequelize-typescript';
import { Product } from './../app/prescription-items/product/product.entity';
import { PrescriptionItemComponent } from './../app/prescription-items/prescription-item-component/prescription-item-component.entity';
import { PrescriptionItemGroup } from './../app/prescription-items/prescription-item-group/prescription-item-group.entity';
import { PrescriptionItemFrequency } from './../app/prescription-items/prescription-item-frequency/prescription-item-frequency.entity';
import { PrescriptionItemDescription } from './../app/prescription-items/prescription-item-description/prescription-item-description.entity';
import { PrescriptionItem } from './../app/prescription-items/prescription-item/prescription-item.entity';
import { AdmissionPrescription } from './../app/admission-prescription/admission-prescription.entity';
import { ProfessionalType } from './../app/backoffice/professional-type/professional-type.entity';
import { ProfessionalBoard } from './../app/backoffice/professional-board/professional-board.entity';
import { AdmissionPrescriptionZone } from './../app/backoffice/admission-prescription-zone/admission-prescription-zone.entity';
import { HealthInsurance } from './../app/health-insurance/health-insurance.entity';
import { AdmissionHealthInsurance } from './../app/admission-health-insurance/admission-health-insurance.entity';
import { PatientAdmission } from './../app/patient-admission/patient-admission.entity';
import { Cid } from './../app/cid/cid.entity';
import { Substance } from './../app/substance/substance.entity';
import { PatientAllergy } from './../app/patient-allergy/patient-allergy.entity';
import { ConfigService } from '../config/config.service';
import { Patient } from '../app/patient/patient.entity';
import { Person } from '../app/person/person.entity';
import { ContactInfo } from './../app/contact-info/contact-info.entity';
import { AdmissionCid } from './../app/admission-cid/admission-cid.entity';
import { HealthcareProfessional } from './../app/backoffice/healthcare-professional/healthcare-professional.entity';
import { ItemUnit } from './../app/prescription-items/item-unit/item-unit.entity';
import { UsageVia } from './../app/prescription-items/usage-via/usage-via.entity';

export const databaseProviders = [
    {
      provide: 'SEQUELIZE',
      useFactory: async (configService: ConfigService) => {
        const sequelize = new Sequelize(configService.sequelizeOrmConfig);
        sequelize.addModels([
          Patient,
          Person,
          ContactInfo,
          PatientAllergy,
          Substance,
          Cid,
          PatientAdmission,
          AdmissionCid,
          AdmissionHealthInsurance,
          HealthInsurance,
          ProfessionalBoard,
          AdmissionPrescriptionZone,
          ProfessionalType,
          HealthcareProfessional,
          AdmissionPrescription,
          PrescriptionItem,
          PrescriptionItemDescription,
          PrescriptionItemFrequency,
          PrescriptionItemGroup,
          PrescriptionItemComponent,
          Product,
          ItemUnit,
          UsageVia
        ]);
        return sequelize;
      },
      inject: [ConfigService]
    },
  ];

export enum Gender {
    female = 'F',
    male = 'M',
    other = 'O',
    undefined = 'U'
}
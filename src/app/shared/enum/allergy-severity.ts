export enum AllergySeverity {
    high = "HIGH",
    moderate = "MODERATE",
    low = "LOW",
    unknown = "UNKNOWN"
}
export enum SubstanceCategory {
    drug = 'DRUG',
    food = 'FOOD',
    others = 'OTHERS'
}
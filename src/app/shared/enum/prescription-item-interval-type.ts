export enum IntervalType {
    day = 'D',
    hour = 'H',
    minute = 'M'
}
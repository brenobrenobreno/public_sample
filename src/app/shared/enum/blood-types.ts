export enum BloodTypes {
    aPositive = 'A+', 
    aNegative = 'A-', 
    bPositive = 'B+', 
    bNegative = 'B-', 
    abPositive = 'AB+', 
    abNegative = 'AB-', 
    oPositive =  'O+', 
    oNegative = 'O-'
}
export enum MaritalStatus {
    annulled = 'ANNULLED',
    divorced = 'DIVORCED',
    domesticPartner = 'DOMESTIC_PARTNER',
    interlocutory = "INTERLOCUTORY",
    legallySeparated = "LEGALLY_SEPARATED",
    married = 'MARRIED',
    neverMarried = 'NEVER_MARRIED',
    polygamous = 'POLYGAMOUS',
    unmarried = 'UNMARRIED',
    widowed = 'WIDOWED',
    unknown = 'UNKNOWN'
}
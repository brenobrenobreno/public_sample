export enum AllergyDeclaration {
    yes = "YES",
    no = "NO",
    undefined = "UNDEFINED"
}
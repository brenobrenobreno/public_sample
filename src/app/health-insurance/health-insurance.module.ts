import { healthInsuranceProvider } from './health-insurance.provider';
import { DatabaseModule } from './../../database/database.module';
import { Module } from '@nestjs/common';
import { HealthInsuranceService } from './health-insurance.service';
import { HealthInsuranceController } from './health-insurance.controller';

@Module({
  imports: [DatabaseModule],
  exports: [...healthInsuranceProvider],
  providers: [HealthInsuranceService, ...healthInsuranceProvider],
  controllers: [HealthInsuranceController]
})
export class HealthInsuranceModule {}

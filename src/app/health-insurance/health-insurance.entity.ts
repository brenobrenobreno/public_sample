import { AdmissionHealthInsurance } from './../admission-health-insurance/admission-health-insurance.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, HasMany } from 'sequelize-typescript';

@Table({
    tableName: 'health_insurance',
})
export class HealthInsurance extends Model<HealthInsurance> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull
    @Column
    name: string;

    @AllowNull
    @Column
    plan: string;

    @HasMany(() => AdmissionHealthInsurance)
    admission_health_insurance: AdmissionHealthInsurance;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}
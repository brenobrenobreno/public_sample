import { HealthInsurance } from './health-insurance.entity';

export const healthInsuranceProvider = [
    {
      provide: 'HEALTH_INSURANCE_REPOSITORY',
      useValue: HealthInsurance,
    },
];
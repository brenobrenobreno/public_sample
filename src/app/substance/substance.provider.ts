import { Substance } from './substance.entity';

export const substancesProvider = [
    {
      provide: 'SUBSTANCES_REPOSITORY',
      useValue: Substance,
    },
  ];
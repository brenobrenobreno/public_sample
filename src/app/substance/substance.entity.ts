import { PatientAllergy } from './../patient-allergy/patient-allergy.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, HasOne, BelongsTo, Default } from 'sequelize-typescript';

@Table({
    tableName: 'substance',
})

export class Substance extends Model<Substance> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull
    @Unique
    @Column
    substance: string;

    @AllowNull
    @Column
    category: string;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}
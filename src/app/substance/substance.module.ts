import { DatabaseModule } from './../../database/database.module';
import { Module } from '@nestjs/common';
import { substancesProvider } from './substance.provider';

@Module({
    imports: [DatabaseModule],
    exports: [...substancesProvider],
    providers: [...substancesProvider]
})
export class SubstanceModule {}

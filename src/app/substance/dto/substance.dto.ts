import { SubstanceCategory } from './../../shared/enum/substance-category';
import { AllergySeverity } from './../../shared/enum/allergy-severity';

export class SubstanceDto {
    id: string;
    external_id: string;
    substance: string;
    category: SubstanceCategory;
}
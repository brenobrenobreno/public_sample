import { UsageVia } from './usage-via.entity';

export const usageViaProvider = [
    {
      provide: 'USAGE_VIA_REPOSITORY',
      useValue: UsageVia,
    },
  ];
import { Test, TestingModule } from '@nestjs/testing';
import { UsageViaService } from './usage-via.service';

describe('UsageViaService', () => {
  let service: UsageViaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsageViaService],
    }).compile();

    service = module.get<UsageViaService>(UsageViaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

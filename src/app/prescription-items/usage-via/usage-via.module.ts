import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { usageViaProvider } from './usage-via.provider';

@Module({
    imports: [DatabaseModule],
    exports: [...usageViaProvider],
    providers: [...usageViaProvider]
})
export class UsageViaModule {}

import { PrescriptionItemComponent } from './../prescription-item-component/prescription-item-component.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, Default, HasMany } from 'sequelize-typescript';

@Table({
    tableName: 'product',
})

export class Product extends Model<Product> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull(false)
    @Column
    name: string;

    @AllowNull
    @Column
    unit: string;

    @HasMany(() => PrescriptionItemComponent, 'product_id')
    prescription_item_component: PrescriptionItemComponent[]

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
    
}

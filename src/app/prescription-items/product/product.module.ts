import { productProvider } from './product.provider';
import { DatabaseModule } from './../../../database/database.module';
import { Module } from '@nestjs/common';

@Module({
    imports: [DatabaseModule],
    exports: [...productProvider],
    providers: [...productProvider]
})
export class ProductModule {}

import { DatabaseModule } from './../../../database/database.module';
import { prescriptionItemDescriptionProvider } from './prescription-item-description.provider';
import { Module } from '@nestjs/common';

@Module({
    imports: [DatabaseModule],
    exports: [...prescriptionItemDescriptionProvider],
    providers: [...prescriptionItemDescriptionProvider]
})
export class PrescriptionItemDescriptionModule {}

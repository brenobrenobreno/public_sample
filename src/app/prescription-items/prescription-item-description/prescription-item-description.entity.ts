import { PrescriptionItem } from './../prescription-item/prescription-item.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, HasMany, Default } from 'sequelize-typescript';

@Table({
    tableName: 'prescription_item_description',
})

export class PrescriptionItemDescription extends Model<PrescriptionItemDescription> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull(false)
    @Column
    description: string;

    @HasMany(() => PrescriptionItem, 'prescription_item_description_id')
    prescription_item: PrescriptionItem[]

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
    
}

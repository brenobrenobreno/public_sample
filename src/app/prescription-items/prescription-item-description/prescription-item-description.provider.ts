import { PrescriptionItemDescription } from './prescription-item-description.entity';

export const prescriptionItemDescriptionProvider = [
    {
      provide: 'PRESCRIPTION_ITEM_DESCRIPTION_REPOSITORY',
      useValue: PrescriptionItemDescription,
    },
  ];
import { DatabaseModule } from './../../../database/database.module';
import { Module } from '@nestjs/common';
import { prescriptionItemComponentProvider } from './prescription-item-component.provider';
import { PrescriptionItemComponentService } from './prescription-item-component.service';
import { PrescriptionItemComponentController } from './prescription-item-component.controller';

@Module({
    imports: [DatabaseModule],
    exports: [...prescriptionItemComponentProvider],
    providers: [...prescriptionItemComponentProvider, PrescriptionItemComponentService],
    controllers: [PrescriptionItemComponentController]
})
export class PrescriptionItemComponentModule {}

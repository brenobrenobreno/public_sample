import { PrescriptionItemComponent } from './prescription-item-component.entity';
import { PrescriptionItemComponentService } from './prescription-item-component.service';
import { Controller, Get, Param } from '@nestjs/common';

@Controller('components')
export class PrescriptionItemComponentController {
    constructor(private readonly componentService: PrescriptionItemComponentService) {}

    @Get()
    findAll(): Promise<PrescriptionItemComponent[]> {
      return this.componentService.findAll();
    }

    @Get('item/:itemId')
    findAllByPatient(@Param('itemId') itemId: string): Promise<PrescriptionItemComponent[]> {
      return this.componentService.findAllByPrescriptionItem(itemId);
    }
  
    @Get(':componentId')
    findOne(@Param('componentId') componentId: string): Promise<PrescriptionItemComponent> {
      return this.componentService.findOne(componentId);
    }
}

import { PrescriptionItemComponent } from './prescription-item-component.entity';

export const prescriptionItemComponentProvider = [
    {
      provide: 'PRESCRIPTION_ITEM_COMPONENT_REPOSITORY',
      useValue: PrescriptionItemComponent,
    },
];

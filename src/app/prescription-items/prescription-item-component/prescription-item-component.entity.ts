import { PrescriptionItem } from './../prescription-item/prescription-item.entity';
import { Product } from './../product/product.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, Default, ForeignKey, BelongsTo } from 'sequelize-typescript';

@Table({
    tableName: 'prescription_item_component',
})

export class PrescriptionItemComponent extends Model<PrescriptionItemComponent> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @ForeignKey(() => Product)
    @AllowNull(true)
    @Column(DataType.UUIDV4)
    product_id: string;

    @AllowNull
    @Column(DataType.DOUBLE)
    quantity: number;

    @ForeignKey(() => PrescriptionItem)
    @Column(DataType.UUIDV4)
    prescription_item_id: string;

    @BelongsTo(() => Product)
    product: Product

    @BelongsTo(() => PrescriptionItem)
    prescription_item: PrescriptionItem

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
    
}

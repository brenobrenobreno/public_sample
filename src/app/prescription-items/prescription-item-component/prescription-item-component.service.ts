import { Product } from './../product/product.entity';
import { PrescriptionItemComponent } from './prescription-item-component.entity';
import { Injectable, Inject } from '@nestjs/common';

@Injectable()
export class PrescriptionItemComponentService {
    constructor(
        @Inject('PRESCRIPTION_ITEM_COMPONENT_REPOSITORY') private componentProvider: typeof PrescriptionItemComponent
    ) {}

    async findAll(): Promise<PrescriptionItemComponent[]> {
        const components = await this.componentProvider.findAll(
            {
                include: [
                    {
                        model: Product,
                        as: 'product',
                        attributes: ['external_id', 'name', 'unit']
                    }
                ]
            }
        );
        return components;
    }

    async findAllByPrescriptionItem(itemId: string): Promise<PrescriptionItemComponent[]> {
        const components = await this.componentProvider.findAll(
            {
                where: {
                    prescription_item_id: itemId
                },
                include: [
                    {
                        model: Product,
                        as: 'product',
                        attributes: ['external_id', 'name', 'unit']
                    }
                ]
            }
        );
        return components;
    }

    async findOne(componentId: string): Promise<PrescriptionItemComponent> {
        const component = await this.componentProvider.findOne(
            {
                where: {
                    id: componentId
                },
                include: [
                    {
                        model: Product,
                        as: 'product',
                        attributes: ['external_id', 'name', 'unit']
                    }
                ]
            }
        );
        return component;
    }

}

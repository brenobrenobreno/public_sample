import { IntervalType } from './../../shared/enum/prescription-item-interval-type';
import { PrescriptionItem } from './../prescription-item/prescription-item.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, HasMany, Default } from 'sequelize-typescript';

@Table({
    tableName: 'prescription_item_frequency',
})

export class PrescriptionItemFrequency extends Model<PrescriptionItemFrequency> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull(false)
    @Unique
    @Column
    frequency: string;

    @AllowNull
    @Column(DataType.DOUBLE)
    value: number;

    @AllowNull
    @Column
    fixed: boolean;

    @AllowNull
    @Default(DataType.ENUM(IntervalType.hour))
    @Column({ type: 
        DataType.ENUM(
            IntervalType.day,
            IntervalType.hour,
            IntervalType.minute) 
        })
    type: IntervalType;

    @HasMany(() => PrescriptionItem, 'frequency_id')
    prescription_item: PrescriptionItem[]

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
    
}

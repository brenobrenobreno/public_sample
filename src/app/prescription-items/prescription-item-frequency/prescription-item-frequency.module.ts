import { prescriptionItemFrequencyProvider } from './prescription-item-frequency.provider';
import { DatabaseModule } from './../../../database/database.module';
import { Module } from '@nestjs/common';

@Module({
    imports: [DatabaseModule],
    exports: [...prescriptionItemFrequencyProvider],
    providers: [...prescriptionItemFrequencyProvider]
})
export class PrescriptionItemFrequencyModule {}

import { PrescriptionItemFrequency } from './prescription-item-frequency.entity';

export const prescriptionItemFrequencyProvider = [
    {
      provide: 'PRESCRIPTION_ITEM_FREQUENCY_REPOSITORY',
      useValue: PrescriptionItemFrequency,
    },
  ];
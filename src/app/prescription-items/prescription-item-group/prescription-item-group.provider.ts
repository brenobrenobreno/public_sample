import { PrescriptionItemGroup } from './prescription-item-group.entity';

export const prescriptionItemGroupProvider = [
    {
      provide: 'PRESCRIPTION_ITEM_GROUP_REPOSITORY',
      useValue: PrescriptionItemGroup,
    },
  ];
import { prescriptionItemGroupProvider } from './prescription-item-group.provider';
import { DatabaseModule } from './../../../database/database.module';
import { Module } from '@nestjs/common';

@Module({
    imports: [DatabaseModule],
    exports: [...prescriptionItemGroupProvider],
    providers: [...prescriptionItemGroupProvider]
})
export class PrescriptionItemGroupModule {}

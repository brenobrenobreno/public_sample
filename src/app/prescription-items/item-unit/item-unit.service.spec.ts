import { Test, TestingModule } from '@nestjs/testing';
import { ItemUnitService } from './item-unit.service';

describe('ItemUnitService', () => {
  let service: ItemUnitService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ItemUnitService],
    }).compile();

    service = module.get<ItemUnitService>(ItemUnitService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

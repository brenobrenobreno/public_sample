import { ItemUnit } from './item-unit.entity';

export const itemUnitProvider = [
    {
      provide: 'ITEM_UNIT_REPOSITORY',
      useValue: ItemUnit,
    },
  ];
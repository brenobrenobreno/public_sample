import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, Default } from 'sequelize-typescript';

@Table({
    tableName: 'item_unit',
})
export class ItemUnit extends Model<ItemUnit> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;

    @Unique
    @AllowNull(false)
    @Column
    name: string;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}
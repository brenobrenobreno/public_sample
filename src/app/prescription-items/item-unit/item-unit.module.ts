import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { itemUnitProvider } from './item-unit.provider';

@Module({
    imports: [DatabaseModule],
    exports: [...itemUnitProvider],
    providers: [...itemUnitProvider]
})
export class ItemUnitModule {}

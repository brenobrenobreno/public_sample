import { PrescriptionItem } from './prescription-item.entity';

export const prescriptionItemProvider = [
    {
      provide: 'PRESCRIPTION_ITEM_REPOSITORY',
      useValue: PrescriptionItem,
    },
  ];
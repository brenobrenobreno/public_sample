import { PrescriptionItem } from './prescription-item.entity';
import { PrescriptionItemService } from './prescription-item.service';
import { Controller, Get, Param } from '@nestjs/common';

@Controller('prescription-items')
export class PrescriptionItemController {
    constructor(private readonly prescriptionItemService: PrescriptionItemService) {}

    @Get()
    findAll(): Promise<PrescriptionItem[]> {
      return this.prescriptionItemService.findAll();
    }

    @Get('prescription/:admissionPrescriptionId')
    findAllByAdmissionPrescription(@Param('admissionPrescriptionId') admissionPrescriptionId: string): Promise<PrescriptionItem[]> {
      return this.prescriptionItemService.findAllByAdmissionPrescription(admissionPrescriptionId);
    }
  
    @Get(':prescriptionItemId')
    findOne(@Param('prescriptionItemId') prescriptionItemId: string): Promise<PrescriptionItem> {
      return this.prescriptionItemService.findOne(prescriptionItemId);
    }
}

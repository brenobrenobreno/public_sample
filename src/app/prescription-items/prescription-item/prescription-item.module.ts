import { DatabaseModule } from './../../../database/database.module';
import { Module } from '@nestjs/common';
import { prescriptionItemProvider } from './prescription-item.provider';
import { PrescriptionItemService } from './prescription-item.service';
import { PrescriptionItemController } from './prescription-item.controller';

@Module({
    imports: [DatabaseModule],
    exports: [...prescriptionItemProvider],
    providers: [...prescriptionItemProvider, PrescriptionItemService],
    controllers: [PrescriptionItemController]
})
export class PrescriptionItemModule {}

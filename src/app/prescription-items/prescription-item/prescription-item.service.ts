import { Person } from './../../person/person.entity';
import { AdmissionPrescription } from './../../admission-prescription/admission-prescription.entity';
import { HealthcareProfessional } from './../../backoffice/healthcare-professional/healthcare-professional.entity';
import { PrescriptionItemFrequency } from './../prescription-item-frequency/prescription-item-frequency.entity';
import { PrescriptionItemDescription } from './../prescription-item-description/prescription-item-description.entity';
import { PrescriptionItemGroup } from './../prescription-item-group/prescription-item-group.entity';
import { PrescriptionItem } from './prescription-item.entity';
import { Injectable, Inject } from '@nestjs/common';

@Injectable()
export class PrescriptionItemService {
    constructor(
        @Inject('PRESCRIPTION_ITEM_REPOSITORY') private prescriptionItemProvider: typeof PrescriptionItem
    ) {}

    async findAll(): Promise<PrescriptionItem[]> {
        const prescriptionItems = await this.prescriptionItemProvider.findAll({
            include: [
                {
                    model: PrescriptionItemDescription,
                    as: 'prescription_item_description',
                    attributes: ['external_id', 'description']
                },
                {
                    model: PrescriptionItemGroup,
                    as: 'prescription_item_group',
                    attributes: ['external_id', 'name']
                },
                {
                    model: PrescriptionItemFrequency,
                    as: 'prescription_item_frequency',
                    attributes: ['external_id', 'frequency', 'value', 'fixed', 'type']
                },
                {
                    model: HealthcareProfessional,
                    as: 'healthcare_professional',
                    attributes: ['external_id', 'board_code'],
                    include: [
                        {
                            model: Person,
                            as: 'personal_info',
                            attributes: ['name']
                        }
                    ]
                },
                {
                    model: AdmissionPrescription,
                    as: 'admission_prescription',
                    attributes: ['external_id', 'admission_prescription_cancelled', 'initial_datetime', 'final_datetime', 'patient_admission_id']
                },
            ]
        });
        return prescriptionItems;
    }

    async findAllByAdmissionPrescription(admissionPrescriptionId: string): Promise<PrescriptionItem[]> {
        const prescriptionItems = await this.prescriptionItemProvider.findAll({
            where: {
                admission_prescription_id: admissionPrescriptionId
            },
            include: [
                {
                    model: PrescriptionItemDescription,
                    as: 'prescription_item_description',
                    attributes: ['external_id', 'description']
                },
                {
                    model: PrescriptionItemGroup,
                    as: 'prescription_item_group',
                    attributes: ['external_id', 'name']
                },
                {
                    model: PrescriptionItemFrequency,
                    as: 'prescription_item_frequency',
                    attributes: ['external_id', 'frequency', 'value', 'fixed', 'type']
                },
                {
                    model: HealthcareProfessional,
                    as: 'healthcare_professional',
                    attributes: ['external_id', 'board_code'],
                    include: [
                        {
                            model: Person,
                            as: 'personal_info',
                            attributes: ['name']
                        }
                    ]
                },
                {
                    model: AdmissionPrescription,
                    as: 'admission_prescription',
                    attributes: ['external_id', 'admission_prescription_cancelled', 'initial_datetime', 'final_datetime', 'patient_admission_id']
                },
            ]
        });
        return prescriptionItems;
    }

    async findOne(prescriptionId: string): Promise<PrescriptionItem> {
        const prescriptionItem = await this.prescriptionItemProvider.findOne({
            where :{
                id: prescriptionId
            },
            include: [
                {
                    model: PrescriptionItemDescription,
                    as: 'prescription_item_description',
                    attributes: ['external_id', 'description']
                },
                {
                    model: PrescriptionItemGroup,
                    as: 'prescription_item_group',
                    attributes: ['external_id', 'name']
                },
                {
                    model: PrescriptionItemFrequency,
                    as: 'prescription_item_frequency',
                    attributes: ['external_id', 'frequency', 'value', 'fixed', 'type']
                },
                {
                    model: HealthcareProfessional,
                    as: 'healthcare_professional',
                    attributes: ['external_id', 'board_code'],
                    include: [
                        {
                            model: Person,
                            as: 'personal_info',
                            attributes: ['name']
                        }
                    ]
                },
                {
                    model: AdmissionPrescription,
                    as: 'admission_prescription',
                    attributes: ['external_id', 'admission_prescription_cancelled', 'initial_datetime', 'final_datetime', 'patient_admission_id']
                },
            ]
        });
        return prescriptionItem;
    }

}

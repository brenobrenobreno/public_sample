import { PrescriptionItemComponent } from './../prescription-item-component/prescription-item-component.entity';
import { PrescriptionItemFrequency } from './../prescription-item-frequency/prescription-item-frequency.entity';
import { PrescriptionItemGroup } from '../prescription-item-group/prescription-item-group.entity';
import { PrescriptionItemDescription } from './../prescription-item-description/prescription-item-description.entity';
import { AdmissionPrescription } from './../../admission-prescription/admission-prescription.entity';
import { HealthcareProfessional } from './../../backoffice/healthcare-professional/healthcare-professional.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, BelongsTo, Default, HasMany } from 'sequelize-typescript';

@Table({
    tableName: 'prescription_item',
})

export class PrescriptionItem extends Model<PrescriptionItem> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @ForeignKey(() => PrescriptionItemGroup)
    @Column(DataType.UUIDV4)
    prescription_item_group_id: string;

    @ForeignKey(() => PrescriptionItemDescription)
    @Column(DataType.UUIDV4)
    prescription_item_description_id: string;

    @AllowNull
    @Column(DataType.DOUBLE)
    quantity: number;

    @AllowNull
    @Column(DataType.TEXT)
    unit: string;

    @AllowNull
    @Column(DataType.TEXT)
    comments: string;

    @AllowNull
    @Column
    application_method: string;

    @ForeignKey(() => PrescriptionItemFrequency)
    @Column(DataType.UUIDV4)
    frequency_id: string;

    @AllowNull
    @ForeignKey(() => HealthcareProfessional)
    @Column(DataType.UUIDV4)
    healthcare_professional_id: string;

    @ForeignKey(() => AdmissionPrescription)
    @Column(DataType.UUIDV4)
    admission_prescription_id: string;

    @BelongsTo(() => PrescriptionItemGroup)
    prescription_item_group: PrescriptionItemGroup

    @BelongsTo(() => PrescriptionItemFrequency)
    prescription_item_frequency: PrescriptionItemFrequency

    @BelongsTo(() => PrescriptionItemDescription)
    prescription_item_description: PrescriptionItemDescription

    @BelongsTo(() => HealthcareProfessional)
    healthcare_professional: HealthcareProfessional

    @BelongsTo(() => AdmissionPrescription)
    admission_prescription: AdmissionPrescription

    @HasMany(() => PrescriptionItemComponent, 'prescription_item_id')
    prescription_item_components: PrescriptionItemComponent[]

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}
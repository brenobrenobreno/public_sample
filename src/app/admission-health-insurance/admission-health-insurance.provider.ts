import { AdmissionHealthInsurance } from './admission-health-insurance.entity';

export const admissionHealthInsuranceProvider = [
    {
      provide: 'ADMISSION_HEALTH_INSURANCE_REPOSITORY',
      useValue: AdmissionHealthInsurance,
    },
];
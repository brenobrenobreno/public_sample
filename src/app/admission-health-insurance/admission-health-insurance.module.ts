import { DatabaseModule } from './../../database/database.module';
import { Module } from '@nestjs/common';
import { AdmissionHealthInsuranceController } from './admission-health-insurance.controller';
import { admissionHealthInsuranceProvider } from './admission-health-insurance.provider';
import { AdmissionHealthInsuranceService } from './admission-health-insurance.service';

@Module({
  imports: [DatabaseModule],
  exports: [...admissionHealthInsuranceProvider],
  controllers: [AdmissionHealthInsuranceController],
  providers: [AdmissionHealthInsuranceService, ...admissionHealthInsuranceProvider]
})
export class AdmissionHealthInsuranceModule {}

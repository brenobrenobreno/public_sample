import { HealthInsurance } from './../health-insurance/health-insurance.entity';
import { PatientAdmission } from './../patient-admission/patient-admission.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, NotNull, BelongsTo, HasMany } from 'sequelize-typescript';

@Table({
    tableName: 'admission_health_insurance',
})
export class AdmissionHealthInsurance extends Model<AdmissionHealthInsurance> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull
    @Column
    health_insurance_holder: string;

    @AllowNull
    @Column
    health_insurance_validity: string;

    @AllowNull
    @Column
    patient_health_insurance_register: string;

    @ForeignKey(() => PatientAdmission)
    @Column
    patient_admission_id: string;

    @ForeignKey(() => HealthInsurance)
    @Column
    health_insurance_id: string;

    @BelongsTo(() => HealthInsurance)
    health_insurance: HealthInsurance;

    @BelongsTo(() => PatientAdmission)
    patient_admission: PatientAdmission;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}
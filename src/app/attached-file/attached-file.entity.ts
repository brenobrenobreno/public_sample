import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, ForeignKey, NotNull, BelongsTo } from 'sequelize-typescript';
import { ContactInfo } from '../contact-info/contact-info.entity';

@Table({
    tableName: 'attached_file',
})
export class AttachedFile extends Model<AttachedFile> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @NotNull
    @Column
    name: string;

    @NotNull
    @Column
    path: string;

    @ForeignKey(() => ContactInfo)
    @Column
    contact_info_id: string;

    @BelongsTo(() => ContactInfo)
    contact_info: ContactInfo;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}


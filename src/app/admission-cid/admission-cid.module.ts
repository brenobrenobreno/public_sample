import { DatabaseModule } from './../../database/database.module';
import { Module } from '@nestjs/common';
import { AdmissionCidController } from './admission-cid.controller';
import { AdmissionCidService } from './admission-cid.service';
import { admissionCidProvider } from './admission-cid.provider';

@Module({
  imports: [DatabaseModule],
  exports: [...admissionCidProvider],
  controllers: [AdmissionCidController],
  providers: [AdmissionCidService, ...admissionCidProvider]
})
export class AdmissionCidModule {}

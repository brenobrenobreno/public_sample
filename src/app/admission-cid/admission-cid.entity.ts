import { PatientAdmission } from './../patient-admission/patient-admission.entity';
import { Cid } from './../cid/cid.entity';
import { AdmissionState } from './../shared/enum/admission-state';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, NotNull, BelongsTo } from 'sequelize-typescript';

@Table({
    tableName: 'admission_cid',
})
export class AdmissionCid extends Model<AdmissionCid> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @ForeignKey(() => Cid)
    cid_id: string;

    @AllowNull
    @Column({ type: 
        DataType.ENUM(
            AdmissionState.active, 
            AdmissionState.inactive)
        })
    admission_cid_state: AdmissionState;

    @ForeignKey(() => PatientAdmission)
    @Column
    patient_admission_id: string;

    @BelongsTo(() => PatientAdmission)
    patientAdmission: PatientAdmission;
    
    @BelongsTo(() => Cid)
    cid: Cid;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

import { AdmissionCid } from './admission-cid.entity';

export const admissionCidProvider = [
    {
      provide: 'ADMISSION_CID_REPOSITORY',
      useValue: AdmissionCid,
    },
  ];
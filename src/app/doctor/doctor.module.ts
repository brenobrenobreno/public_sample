import { DatabaseModule } from './../../database/database.module';
import { Module } from '@nestjs/common';
import { DoctorController } from './doctor.controller';
import { DoctorService } from './doctor.service';
import { doctorProvider } from './doctor.provider';

@Module({
  imports: [DatabaseModule],
  exports: [...doctorProvider],
  controllers: [DoctorController],
  providers: [DoctorService, ...doctorProvider]
})
export class DoctorModule {}

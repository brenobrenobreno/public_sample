import { Doctor } from './doctor.entity';

export const doctorProvider = [
    {
      provide: 'DOCTOR_REPOSITORY',
      useValue: Doctor,
    },
];
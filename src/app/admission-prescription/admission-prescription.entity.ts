import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, NotNull, HasOne, BelongsTo, HasMany } from 'sequelize-typescript';
import { AdmissionPrescriptionZone } from '../backoffice/admission-prescription-zone/admission-prescription-zone.entity';
import { PatientAdmission } from '../patient-admission/patient-admission.entity';
import { HealthcareProfessional } from '../backoffice/healthcare-professional/healthcare-professional.entity';
import { PrescriptionItem } from '../prescription-items/prescription-item/prescription-item.entity';

@Table({
    tableName: 'admission_prescription',
})
export class AdmissionPrescription extends Model<AdmissionPrescription> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull(false)
    @Column
    admission_prescription_cancelled: boolean;

    @AllowNull(false)
    @Column({ field: 'initial_datetime' })
    initial_datetime: Date;

    @AllowNull(false)
    @Column(DataType.DATEONLY)
    final_datetime: Date;

    @ForeignKey(() => AdmissionPrescriptionZone)
    @Column
    admission_prescription_zone_id: string;

    @ForeignKey(() => PatientAdmission)
    @Column
    patient_admission_id: string;

    @ForeignKey(() => HealthcareProfessional)
    @Column
    prescription_provider_id: string;

    @ForeignKey(() => HealthcareProfessional)
    @Column
    prescription_transcriber_id: string;

    @BelongsTo(() => AdmissionPrescriptionZone)
    admission_prescription_zone: AdmissionPrescriptionZone;

    @BelongsTo(() => PatientAdmission)
    admission_prescription: PatientAdmission;

    @BelongsTo(() => HealthcareProfessional, 'prescription_provider_id')
    provider: HealthcareProfessional;

    @BelongsTo(() => HealthcareProfessional, 'prescription_transcriber_id')
    transcriber: HealthcareProfessional;

    @HasMany(() => PrescriptionItem, 'admission_prescription_id')
    prescription_items: PrescriptionItem[]

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

import { PersonModule } from './../person/person.module';
import { HealthcareProfessionalModule } from './../backoffice/healthcare-professional/healthcare-professional.module';
import { DatabaseModule } from './../../database/database.module';
import { AdmissionPrescriptionService } from './admission-prescription.service';
import { AdmissionPrescriptionController } from './admission-prescription.controller';
import { admissionPrescriptionProvider } from './admission-prescription.provider';

import { Module } from '@nestjs/common';

@Module({
  imports: [DatabaseModule, HealthcareProfessionalModule, PersonModule],
  exports: [AdmissionPrescriptionService, ...admissionPrescriptionProvider],
  providers: [AdmissionPrescriptionService, ...admissionPrescriptionProvider],
  controllers: [AdmissionPrescriptionController]
})
export class AdmissionPrescriptionModule {}

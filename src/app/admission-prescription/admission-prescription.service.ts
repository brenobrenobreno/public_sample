import { ProfessionalBoard } from './../backoffice/professional-board/professional-board.entity';
import { Person } from './../person/person.entity';
import { ProfessionalType } from './../backoffice/professional-type/professional-type.entity';
import { AdmissionPrescription } from './admission-prescription.entity';
import { HealthcareProfessional } from './../backoffice/healthcare-professional/healthcare-professional.entity';
import { AdmissionPrescriptionZone } from './../backoffice/admission-prescription-zone/admission-prescription-zone.entity';
import { Injectable, Inject } from '@nestjs/common';
import { PrescriptionItem } from '../prescription-items/prescription-item/prescription-item.entity';
import { PrescriptionItemGroup } from '../prescription-items/prescription-item-group/prescription-item-group.entity';
import { PrescriptionItemFrequency } from '../prescription-items/prescription-item-frequency/prescription-item-frequency.entity';
import { PrescriptionItemDescription } from '../prescription-items/prescription-item-description/prescription-item-description.entity';
import { PrescriptionItemComponent } from '../prescription-items/prescription-item-component/prescription-item-component.entity';
import { Product } from '../prescription-items/product/product.entity';

@Injectable()
export class AdmissionPrescriptionService {
    constructor(
        @Inject('ADMISSION_PRESCRIPTION_REPOSITORY') private prescriptionProvider: typeof AdmissionPrescription
    ) {}

    async findAll(): Promise<AdmissionPrescription[]> {
        const prescriptions = await this.prescriptionProvider.findAll({ 
            include: [
                {
                    model: AdmissionPrescriptionZone,
                    attributes: ['id', 'external_id', 'name']
                },
                {
                    model: HealthcareProfessional,
                    as: 'provider',
                    foreignKey: 'prescription_provider_id',
                    attributes: ['id', 'external_id', 'board_code'],
                    include: [
                        {
                            model: Person,
                            attributes: ['id', 'name', 'social_name', 'gender', 'birthday'],
                            as: 'personal_info'  
                        },
                        {
                            model: ProfessionalType,
                            attributes: ['id', 'external_id', 'name'],
                            as: 'professional_type',
                            include: [{
                                model: ProfessionalBoard,
                                attributes: ['id', 'external_id', 'name', 'region'],
                                as: 'professional_board'
                            }]
                        }
                    ]
                },
                {
                    model: HealthcareProfessional,
                    as: 'transcriber',
                    foreignKey: 'prescription_transcriber_id',
                    attributes: ['id', 'external_id', 'board_code'],
                    include: [
                        {
                            model: Person,
                            as: 'personal_info',
                            attributes: ['id', 'name', 'social_name', 'gender', 'birthday']
                        },
                        {
                            model: ProfessionalType,
                            as: 'professional_type',
                            attributes: ['id', 'external_id', 'name'],
                            include: [{
                                model: ProfessionalBoard,
                                as: 'professional_board',
                                attributes: ['id', 'external_id', 'name', 'region']
                            }]
                    }]
                }
            ]
        });
        return prescriptions;
    }
    

    async findAllByAdmission(admissionId: string): Promise<AdmissionPrescription[]> {
        const prescriptions = await this.prescriptionProvider.findAll({
            where: {
                patient_admission_id: admissionId
            },
            include: [
                {
                    model: AdmissionPrescriptionZone,
                    attributes: ['id', 'external_id', 'name']
                },
                {
                    model: HealthcareProfessional,
                    as: 'provider',
                    foreignKey: 'prescription_provider_id',
                    attributes: ['id', 'external_id', 'board_code'],
                    include: [
                        {
                            model: Person,
                            attributes: ['id', 'name', 'social_name', 'gender', 'birthday'],
                            as: 'personal_info'  
                        },
                        {
                            model: ProfessionalType,
                            attributes: ['id', 'external_id', 'name'],
                            as: 'professional_type',
                            include: [{
                                model: ProfessionalBoard,
                                attributes: ['id', 'external_id', 'name', 'region'],
                                as: 'professional_board'
                            }]
                        }
                    ]
                },
                {
                    model: HealthcareProfessional,
                    as: 'transcriber',
                    foreignKey: 'prescription_transcriber_id',
                    attributes: ['id', 'external_id', 'board_code'],
                    include: [
                        {
                            model: Person,
                            as: 'personal_info',
                            attributes: ['id', 'name', 'social_name', 'gender', 'birthday']
                        },
                        {
                            model: ProfessionalType,
                            as: 'professional_type',
                            attributes: ['id', 'external_id', 'name'],
                            include: [{
                                model: ProfessionalBoard,
                                as: 'professional_board',
                                attributes: ['id', 'external_id', 'name', 'region']
                            }]
                    }]
                },
                {
                    model: PrescriptionItem,
                    as: 'prescription_items',
                    attributes: ['id', 'external_id', 'quantity', 'unit', 'comments', 'application_method'],
                    include: [
                        {
                            model: PrescriptionItemGroup,
                            as: 'prescription_item_group',
                            attributes: ['id', 'external_id', 'name']
                        },
                        {
                            model: PrescriptionItemFrequency,
                            as: 'prescription_item_frequency',
                            attributes: ['id', 'external_id', 'frequency', 'value', 'fixed', 'type']
                        },
                        {
                            model: PrescriptionItemDescription,
                            as: 'prescription_item_description',
                            attributes: ['id', 'external_id', 'description']
                        },
                        {
                            model: PrescriptionItemComponent,
                            as: 'prescription_item_components',
                            attributes: ['id', 'external_id', 'quantity'],
                            include: [
                                {
                                    model: Product,
                                    as: 'product',
                                    attributes: ['id', 'external_id', 'name', 'unit']
                                }
                            ]
                        }
                    ]
                }
            ]
        });
        return prescriptions;
    }

    async findOne(prescriptionId: string): Promise<AdmissionPrescription> {
        const prescription = await this.prescriptionProvider.findOne({
            where: {
                id: prescriptionId
            },
            include: [
                {
                    model: AdmissionPrescriptionZone,
                    attributes: ['id', 'external_id', 'name']
                },
                {
                    model: HealthcareProfessional,
                    as: 'provider',
                    foreignKey: 'prescription_provider_id',
                    attributes: ['id', 'external_id', 'board_code'],
                    include: [
                        {
                            model: Person,
                            attributes: ['id', 'name', 'social_name', 'gender', 'birthday'],
                            as: 'personal_info'  
                        },
                        {
                            model: ProfessionalType,
                            attributes: ['id', 'external_id', 'name'],
                            as: 'professional_type',
                            include: [{
                                model: ProfessionalBoard,
                                attributes: ['id', 'external_id', 'name', 'region'],
                                as: 'professional_board'
                            }]
                        }
                    ]
                },
                {
                    model: HealthcareProfessional,
                    as: 'transcriber',
                    foreignKey: 'prescription_transcriber_id',
                    attributes: ['id', 'external_id', 'board_code'],
                    include: [
                        {
                            model: Person,
                            as: 'personal_info',
                            attributes: ['id', 'name', 'social_name', 'gender', 'birthday']
                        },
                        {
                            model: ProfessionalType,
                            as: 'professional_type',
                            attributes: ['id', 'external_id', 'name'],
                            include: [{
                                model: ProfessionalBoard,
                                as: 'professional_board',
                                attributes: ['id', 'external_id', 'name', 'region']
                            }]
                    }]
                }
            ]
        });
        return prescription;
    }
}

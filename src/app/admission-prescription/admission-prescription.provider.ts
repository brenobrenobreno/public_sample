import { AdmissionPrescription } from './admission-prescription.entity';

export const admissionPrescriptionProvider = [
    {
      provide: 'ADMISSION_PRESCRIPTION_REPOSITORY',
      useValue: AdmissionPrescription,
    },
];
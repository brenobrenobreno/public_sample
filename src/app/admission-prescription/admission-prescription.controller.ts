import { AdmissionPrescription } from './admission-prescription.entity';
import { AdmissionPrescriptionService } from './admission-prescription.service';
import { Controller, Get, Param } from '@nestjs/common';

@Controller('prescriptions')
export class AdmissionPrescriptionController {
    constructor(private readonly admissionPrescriptionService: AdmissionPrescriptionService) {}

    @Get()
    findAll(): Promise<AdmissionPrescription[]> {
      return this.admissionPrescriptionService.findAll();
    }

    @Get('admission/:admissionId')
    findAllByAdmission(@Param('admissionId') admissionId: string): Promise<AdmissionPrescription[]> {
      return this.admissionPrescriptionService.findAllByAdmission(admissionId);
    }

    @Get(':prescriptionId')
    findOne(@Param('prescriptionId') prescriptionId: string): Promise<{}> {
      return this.admissionPrescriptionService.findOne(prescriptionId);
    }
}

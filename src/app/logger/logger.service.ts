import { Injectable } from '@nestjs/common';

@Injectable()
export class LoggerService {
    log(message: string) {
        //Generate current date & time
        const timeElapsed = Date.now();
        const today = new Date(timeElapsed);

        //Print message
        console.log(`[${today}] ${message}`);
    }
}

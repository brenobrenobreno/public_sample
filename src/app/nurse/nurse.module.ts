import { nurseProvider } from './nurse.provider';
import { DatabaseModule } from './../../database/database.module';
import { Module } from '@nestjs/common';
import { NurseService } from './nurse.service';
import { NurseController } from './nurse.controller';

@Module({
  imports: [DatabaseModule],
  exports: [...nurseProvider],
  providers: [NurseService, ...nurseProvider],
  controllers: [NurseController]
})
export class NurseModule {}

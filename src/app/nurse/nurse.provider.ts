import { Nurse } from './nurse.entity';

export const nurseProvider = [
    {
      provide: 'NURSE_REPOSITORY',
      useValue: Nurse,
    },
];
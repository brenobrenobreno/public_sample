import { Patient } from './../patient/patient.entity';
import { AdmissionHealthInsurance } from './../admission-health-insurance/admission-health-insurance.entity';
import { AdmissionCid } from './../admission-cid/admission-cid.entity';
import { PatientAdmission } from './patient-admission.entity';
import { HealthInsurance } from './../health-insurance/health-insurance.entity';
import { Cid } from './../cid/cid.entity';
import { Injectable, Inject } from '@nestjs/common';

@Injectable()
export class PatientAdmissionService {
    constructor(
        @Inject('PATIENT_ADMISSION_REPOSITORY') private admissionProvider: typeof PatientAdmission
    ) {}

    // TODO
    create(admissionObjects: [PatientAdmission, AdmissionCid, AdmissionHealthInsurance]): Promise<void> {
        return null;
    }

    // TODO
    update(admissionObjects: [PatientAdmission, AdmissionCid, AdmissionHealthInsurance]): Promise<void> {
        return null;
    }

    // Generic search

    async findAll(): Promise<PatientAdmission[]> {
        const admissions = await this.admissionProvider.findAll(
            {
                include: [
                    {
                        model: Patient,
                        as: 'patient',
                        attributes: ['id', 'external_id', 'affiliation', 'blood_type', 'civilly_responsible'],
                    },
                    {
                        model: AdmissionCid,
                        as: 'admission_cid',
                        attributes: ['id', 'external_id', 'admission_cid_state'],
                        include: [
                            {
                                model: Cid,
                                as: 'cid',
                                attributes: ['id', 'cid_code', 'cid_description']
                            }
                        ]
                    },
                    {
                        model: AdmissionHealthInsurance,
                        as: 'admission_health_insurance',
                        attributes: ['id', 'external_id', 'health_insurance_holder', 'health_insurance_validity', 'patient_health_insurance_register'],
                        include: [
                            {
                                model: HealthInsurance,
                                as: 'health_insurance',
                                attributes: ['id', 'external_id', 'name', 'plan']
                            }
                        ]
                    },
                ]
            }
        );
        return admissions;
    }

    async findOne(admission_id: string): Promise<PatientAdmission> {
        const admission = await this.admissionProvider.findOne(
            {
                where: {
                    id: admission_id
                },
                include: [
                    {
                        model: Patient,
                        as: 'patient',
                        attributes: ['id', 'external_id', 'affiliation', 'blood_type', 'civilly_responsible'],
                    },
                    {
                        model: AdmissionCid,
                        as: 'admission_cid',
                        attributes: ['id', 'external_id', 'admission_cid_state'],
                        include: [
                            {
                                model: Cid,
                                as: 'cid',
                                attributes: ['id', 'cid_code', 'cid_description']
                            }
                        ]
                    },
                    {
                        model: AdmissionHealthInsurance,
                        as: 'admission_health_insurance',
                        attributes: ['id', 'external_id', 'health_insurance_holder', 'health_insurance_validity', 'patient_health_insurance_register'],
                        include: [
                            {
                                model: HealthInsurance,
                                as: 'health_insurance',
                                attributes: ['id', 'external_id', 'name', 'plan']
                            }
                        ]
                    },
                ]
            }
        );
        return admission;
    }

    // Search by Patient ID

    async findAllByPatient(patient_id: string): Promise<PatientAdmission[]> {
        const admission = await this.admissionProvider.findAll(
            {
                where: {
                    patient_id: patient_id
                },
                include: [
                    {
                        model: Patient,
                        as: 'patient',
                        attributes: ['id', 'external_id', 'affiliation', 'blood_type', 'civilly_responsible'],
                    },
                    {
                        model: AdmissionCid,
                        as: 'admission_cid',
                        attributes: ['id', 'external_id', 'admission_cid_state'],
                        include: [
                            {
                                model: Cid,
                                as: 'cid',
                                attributes: ['id', 'cid_code', 'cid_description']
                            }
                        ]
                    },
                    {
                        model: AdmissionHealthInsurance,
                        as: 'admission_health_insurance',
                        attributes: ['id', 'external_id', 'health_insurance_holder', 'health_insurance_validity', 'patient_health_insurance_register'],
                        include: [
                            {
                                model: HealthInsurance,
                                as: 'health_insurance',
                                attributes: ['id', 'external_id', 'name', 'plan']
                            }
                        ]
                    },
                ]
            }
        );
        return admission;
    }

    async remove(admission_id: string): Promise<void> {
        const admission = await this.admissionProvider.findOne({ where: { id: admission_id } });
        await admission.destroy();
    }
}

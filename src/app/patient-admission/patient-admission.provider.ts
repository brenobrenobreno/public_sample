import { PatientAdmission } from './patient-admission.entity';

export const patientAdmissionProvider = [
    {
      provide: 'PATIENT_ADMISSION_REPOSITORY',
      useValue: PatientAdmission,
    },
  ];
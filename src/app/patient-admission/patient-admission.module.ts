import { AdmissionHealthInsuranceModule } from './../admission-health-insurance/admission-health-insurance.module';
import { AdmissionCidModule } from './../admission-cid/admission-cid.module';
import { DatabaseModule } from './../../database/database.module';
import { PatientAdmissionController } from './patient-admission.controller';
import { PatientAdmissionService } from './patient-admission.service';
import { patientAdmissionProvider } from './patient-admission.provider';

import { Module } from '@nestjs/common';

@Module({
  imports: [DatabaseModule, AdmissionCidModule, AdmissionHealthInsuranceModule],
  exports: [...patientAdmissionProvider],
  controllers: [PatientAdmissionController],
  providers: [PatientAdmissionService, ...patientAdmissionProvider]
})
export class PatientAdmissionModule {}

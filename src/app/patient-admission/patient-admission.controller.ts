import { Person } from './../person/person.entity';
import { AdmissionHealthInsurance } from './../admission-health-insurance/admission-health-insurance.entity';
import { AdmissionCid } from './../admission-cid/admission-cid.entity';
import { PatientAdmission } from './patient-admission.entity';
import { PatientAdmissionService } from './patient-admission.service';

import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';

@Controller('admissions')
export class PatientAdmissionController {
    constructor(private readonly patientAdmissionService: PatientAdmissionService) {}

    /*
    @Post()
    create(@Body() admissionObjects: [PatientAdmission, AdmissionCid, AdmissionHealthInsurance]): Promise<any> {
      return this.patientAdmissionService.create(admissionObjects);
    }

    @Put()
    update(@Body() admissionObjects: [PatientAdmission, AdmissionCid, AdmissionHealthInsurance]): Promise<any> {
      return this.patientAdmissionService.update(admissionObjects);
    }
    */

    @Get()
    findAll(): Promise<PatientAdmission[]> {
      return this.patientAdmissionService.findAll();
    }

    @Get('patient/:patientId')
    findAllByPatient(@Param('patientId') patientId: string): Promise<PatientAdmission[]> {
      return this.patientAdmissionService.findAllByPatient(patientId);
    }
  
    @Get(':admissionId')
    findOne(@Param('admissionId') admissionId: string): Promise<PatientAdmission> {
      return this.patientAdmissionService.findOne(admissionId);
    }
  
    /*
    @Delete(':admissionId')
    remove(@Param('admissionId') admissionId: string): Promise<void> {
      return this.patientAdmissionService.remove(admissionId);
    }
    */
}

import { AdmissionHealthInsurance } from './../admission-health-insurance/admission-health-insurance.entity';
import { AdmissionCid } from './../admission-cid/admission-cid.entity';
import { Patient } from './../patient/patient.entity';
import { AdmissionState } from './../shared/enum/admission-state';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, BelongsTo, HasMany } from 'sequelize-typescript';

@Table({
    tableName: 'patient_admission',
})
export class PatientAdmission extends Model<PatientAdmission> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull
    @Column
    patient_admission_datetime: Date;

    @AllowNull
    @Column({ type:
        DataType.ENUM(
            AdmissionState.active,
            AdmissionState.inactive)
        })
    patient_admission_state: AdmissionState;

    @AllowNull
    @Column
    palliative_care: boolean;

    @AllowNull
    @Column
    public_health_insurance_register: string;

    @AllowNull
    @Column
    confidential_medical_record: boolean;

    @AllowNull
    @Column
    long_term: boolean;

    @ForeignKey(() => Patient)
    @Column
    patient_id: string;

    @BelongsTo(() => Patient)
    patient: Patient;

    @HasMany(() => AdmissionCid, 'patient_admission_id')
    admission_cid: AdmissionCid

    @HasMany(() => AdmissionHealthInsurance, 'patient_admission_id')
    admission_health_insurance: AdmissionHealthInsurance

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

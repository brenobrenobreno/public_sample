import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, NotNull, HasOne } from 'sequelize-typescript';
import { PatientSupporterType } from '../backoffice/patient-supporter-type/patient-supporter-type.entity';

@Table({
    tableName: 'patient_supporter',
})
export class PatientSupporter extends Model<PatientSupporter> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @ForeignKey(() => PatientSupporterType)
    patient_supporter_type_id: string;

    @HasOne(() => PatientSupporterType)
    patient_supporter_type: PatientSupporterType;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

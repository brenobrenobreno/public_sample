import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, BelongsTo, HasOne, Default } from 'sequelize-typescript';
import { Patient } from '../patient/patient.entity';
import { Gender } from '../shared/enum/gender';
import { MaritalStatus } from '../shared/enum/marital-status';
import { ContactInfo } from './../contact-info/contact-info.entity';
import { HealthcareProfessional } from '../backoffice/healthcare-professional/healthcare-professional.entity';

@Table({
    tableName: 'person',
})
export class Person extends Model<Person> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull
    @Column
    photo_id_code: string;

    @AllowNull
    @Column
    name: string;

    @AllowNull
    @Column
    social_name: string;

    @AllowNull
    @Column({ type:
        DataType.ENUM(
            Gender.male,
            Gender.female,
            Gender.other,
            Gender.undefined)
        })
    gender: Gender;

    @AllowNull
    @Column(DataType.DATEONLY)
    birthday: string;

    @AllowNull
    @Column({ type:
        DataType.ENUM(
            MaritalStatus.annulled,
            MaritalStatus.divorced,
            MaritalStatus.domesticPartner,
            MaritalStatus.interlocutory,
            MaritalStatus.legallySeparated,
            MaritalStatus.married,
            MaritalStatus.neverMarried,
            MaritalStatus.polygamous,
            MaritalStatus.unmarried,
            MaritalStatus.widowed,
            MaritalStatus.unknown)
        })
    marital_status: MaritalStatus;

    @AllowNull
    @Column
    social_security_number: string;

    @AllowNull
    @Column
    alternative_social_security_number: string;

    @AllowNull
    @Column
    zip_code: string;

    @AllowNull
    @Column
    street: string;

    @AllowNull
    @Column
    district: string;

    @AllowNull
    @Column
    city: string;

    @AllowNull
    @Column
    state: string;

    @AllowNull
    @Column
    address_complement: string;

    @ForeignKey(() => ContactInfo)
    @Column
    contact_info_id: string;

    @HasOne(() => Patient, 'person_id')
    patient: Patient;

    @HasOne(() => HealthcareProfessional, 'person_id')
    professional: HealthcareProfessional;

    @BelongsTo(() => ContactInfo)
    contact_info: ContactInfo;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

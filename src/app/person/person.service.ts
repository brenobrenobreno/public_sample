import { Injectable, Inject } from '@nestjs/common';
import { Person } from './person.entity';
import { PersonDto } from './dto/person.dto';

@Injectable()
export class PersonService {

    constructor(
        @Inject('PERSONS_REPOSITORY') private personRepository: typeof Person) {}

    async findAll(): Promise<Person[]> {
        return this.personRepository.findAll();
    }

    create(personDto: PersonDto): Promise<Person> {
        return null;
    }

    async update(id: string, attributesToUpdate: {}): Promise<Person> {
        const person = await this.findOne(id);
        return person.update(attributesToUpdate, {
            where: {
                id,
            }
        });
    }

    findOne(id: string): Promise<Person> {
        return this.personRepository.findOne({
          where: {
            id,
          },
        });
    }

    async remove(id: string): Promise<void> {
        const person = await this.findOne(id);
        await person.destroy();
    }
}

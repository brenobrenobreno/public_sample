import { Person } from './person.entity';

export const personsProvider = [
    {
      provide: 'PERSONS_REPOSITORY',
      useValue: Person,
    },
  ];
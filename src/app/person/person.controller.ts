import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { PersonDto } from './dto/person.dto';
import { Person } from './person.entity';
import { PersonService } from './person.service';

@Controller('person')
export class PersonController {
    constructor(private readonly personService: PersonService) {}

    /*
    @Post()
    create(@Body() personDto: PersonDto): Promise<Person> {
      return this.personService.create(personDto);
    }

    @Put(':id')
    update(@Param('id') id: string, @Body() attributesToUpdate: {}): Promise<Person> {
      return this.personService.update(id, attributesToUpdate);
    }

    @Get()
    findAll(): Promise<Person[]> {
      return this.personService.findAll();
    }
  
    @Get(':id')
    findOne(@Param('id') id: string): Promise<Person> {
      return this.personService.findOne(id);
    }
  
    @Delete(':id')
    remove(@Param('id') id: string): Promise<void> {
      return this.personService.remove(id);
    }
    */

}

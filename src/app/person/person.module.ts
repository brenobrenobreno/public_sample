import { Module } from '@nestjs/common';
import { PersonService } from './person.service';
import { PersonController } from './person.controller';
import { personsProvider } from './person.provider';
import { DatabaseModule } from '../../database/database.module';

@Module({
  imports: [DatabaseModule],
  exports: [PersonService, ...personsProvider],
  providers: [PersonService, ...personsProvider],
  controllers: [PersonController]
})
export class PersonModule {}

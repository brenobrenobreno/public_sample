import { Gender } from '../../shared/enum/gender';
import { MaritalStatus } from '../../shared/enum/marital-status';

export class PersonDto {
    id: string;
    external_id: string;
    photo_id_code: string;
    name: string;
    social_name: string;
    gender: Gender;
    birthday: string;
    marital_status: MaritalStatus;
    social_security_number: string;
    alternative_social_security_number: string;
    zip_code: string;
    street: string;
    district: string;
    city: string;
    state: string;
    address_complement: string;
}

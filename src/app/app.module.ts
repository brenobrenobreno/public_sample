import { PrescriptionItemComponentModule } from './prescription-items/prescription-item-component/prescription-item-component.module';
import { ProductModule } from './prescription-items/product/product.module';
import { PrescriptionItemGroupModule } from './prescription-items/prescription-item-group/prescription-item-group.module';
import { PrescriptionItemFrequencyModule } from './prescription-items/prescription-item-frequency/prescription-item-frequency.module';
import { PrescriptionItemDescriptionModule } from './prescription-items/prescription-item-description/prescription-item-description.module';
import { PrescriptionItemModule } from './prescription-items/prescription-item/prescription-item.module';
import { AdmissionPrescriptionModule } from './admission-prescription/admission-prescription.module';
import { AdmissionPrescriptionZoneModule } from './backoffice/admission-prescription-zone/admission-prescription-zone.module';
import { ProfessionalBoardModule } from './backoffice/professional-board/professional-board.module';
import { ConfigModule } from '../config/config.module'
import { PatientModule } from './patient/patient.module';
import { PersonModule } from './person/person.module';
import { ContactInfoModule } from './contact-info/contact-info.module';
import { PatientAllergyModule } from './patient-allergy/patient-allergy.module';
import { PatientAdmissionModule } from './patient-admission/patient-admission.module';
import { HealthInsuranceModule } from './health-insurance/health-insurance.module';
import { AdmissionHealthInsuranceModule } from './admission-health-insurance/admission-health-insurance.module';
import { SubstanceModule } from './substance/substance.module';
import { DoctorModule } from './doctor/doctor.module';
import { NurseModule } from './nurse/nurse.module';
import { MvIntegrationModule } from './mv-integration/mv-integration.module';
import { LoggerModule } from './logger/logger.module';

import { Module } from '@nestjs/common';

@Module({
  imports: [
    ConfigModule,
    PatientModule,
    PersonModule,
    ContactInfoModule,
    PatientAllergyModule,
    PatientAdmissionModule,
    HealthInsuranceModule,
    AdmissionHealthInsuranceModule,
    SubstanceModule,
    DoctorModule,
    NurseModule,
    MvIntegrationModule,
    LoggerModule,
    ProfessionalBoardModule,
    AdmissionPrescriptionZoneModule,
    AdmissionPrescriptionModule,
    PrescriptionItemModule,
    PrescriptionItemDescriptionModule,
    PrescriptionItemFrequencyModule,
    PrescriptionItemGroupModule,
    PrescriptionItemComponentModule,
    ProductModule
  ],
  providers: [],
})
export class AppModule {}

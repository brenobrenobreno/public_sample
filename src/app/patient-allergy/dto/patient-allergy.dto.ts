import { AllergySeverity } from './../../shared/enum/allergy-severity';

export class PatientAllergyDto {
    id: string;
    external_id: string;
    substance_id: string;
    status: string;
    description: string;
    severity: AllergySeverity;
    patient_id: string;
}
import { PatientAllergy } from './patient-allergy.entity';

export const patientAllergiesProvider = [
    {
      provide: 'PATIENT_ALLERGIES_REPOSITORY',
      useValue: PatientAllergy,
    },
  ];
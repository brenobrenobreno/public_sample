import { Module } from '@nestjs/common';
import { PatientAllergyController } from './patient-allergy.controller';
import { patientAllergiesProvider } from './patient-allergy.provider';
import { PatientAllergyService } from './patient-allergy.service';

@Module({
  exports: [PatientAllergyService, ...patientAllergiesProvider],
  controllers: [PatientAllergyController],
  providers: [PatientAllergyService, ...patientAllergiesProvider]
})
export class PatientAllergyModule {}

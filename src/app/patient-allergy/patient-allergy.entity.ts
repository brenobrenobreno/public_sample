import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, HasOne, BelongsTo, Default } from 'sequelize-typescript';

import { Substance } from './../substance/substance.entity';
import { Patient } from '../patient/patient.entity';
import { AllergyDeclaration } from '../shared/enum/allergy-declaration';
import { AllergySeverity } from './../shared/enum/allergy-severity';

@Table({
    tableName: 'patient_allergy',
})

export class PatientAllergy extends Model<PatientAllergy> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull
    @Column({ type: 
        DataType.ENUM(
            AllergyDeclaration.yes,
            AllergyDeclaration.no,
            AllergyDeclaration.undefined
        )   
    })
    status: AllergyDeclaration;

    @AllowNull
    @Column
    description: string;

    @AllowNull
    @Column({ type: 
        DataType.ENUM(
            AllergySeverity.low,
            AllergySeverity.moderate,
            AllergySeverity.high,
            AllergySeverity.unknown
        )   
    })
    severity: AllergySeverity;

    @ForeignKey(() => Substance)
    @Column
    substance_id: string;

    @ForeignKey(() => Patient)
    @Column
    patient_id: string;

    @BelongsTo(() => Patient)
    patient: Patient;

    @BelongsTo(() => Substance)
    substance: Substance;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}
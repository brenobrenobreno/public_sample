import { SyncPrescriptionItemComponentService } from './routine/sync-prescription-item-component/sync-prescription-item-component.service';
import { SyncPrescriptionItemService } from './routine/sync-prescription-item/sync-prescription-item.service';
import { SyncAdmissionPrescriptionService } from './routine/sync-admission-prescription/sync-admission-prescription.service';
import { SyncHealthcareProfessionalService } from './routine/sync-healthcare-professional/sync-healthcare-professional.service';
import { SyncProfessionalTypeService } from './routine/sync-professional-type/sync-professional-type.service';
import { SyncAdmissionPrescriptionZoneService } from './routine/sync-admission-prescription-zone/sync-admission-prescription-zone.service';
import { SyncSubstanceService } from './routine/sync-substance/sync-substance.service';
import { SyncAllergyService } from './routine/sync-allergy/sync-allergy.service';
import { SyncPatientService } from './routine/sync-patient/sync-patient.service';
import { SyncHealthInsuranceService } from './routine/sync-health-insurance/sync-health-insurance.service';
import { SyncAdmissionService } from './routine/sync-admission/sync-admission.service';
import { SyncCidService } from './provisioning/sync-cid/sync-cid.service';
import { SyncProfessionalBoardService } from './routine/sync-professional-board/sync-professional-board.service';
import { SyncItemUnitService } from './provisioning/sync-unit/sync-unit.service';
import { SyncUsageViaService } from './provisioning/sync-usage/sync-usage-via.service';
import { ExecService } from './exec/exec.service';

import { Controller, Get } from '@nestjs/common';

@Controller('mv-integration')
export class MvIntegrationController {
    constructor(
        private readonly substanceService: SyncSubstanceService,
        private readonly cidService: SyncCidService,
        private readonly itemUnitService: SyncItemUnitService,
        private readonly usageViaService: SyncUsageViaService,
        private readonly allergyService: SyncAllergyService,
        private readonly patientService: SyncPatientService,
        private readonly healthInsuranceService: SyncHealthInsuranceService,
        private readonly admissionService: SyncAdmissionService,
        private readonly professionalBoardService: SyncProfessionalBoardService,
        private readonly admissionPrescriptionZoneService: SyncAdmissionPrescriptionZoneService,
        private readonly professionalTypeService: SyncProfessionalTypeService,
        private readonly healthcareProfessionalService: SyncHealthcareProfessionalService,
        private readonly admissionPrescriptionService: SyncAdmissionPrescriptionService,
        private readonly prescriptionItemService: SyncPrescriptionItemService,
        private readonly prescriptionItemComponentService: SyncPrescriptionItemComponentService,
        private readonly execService: ExecService
    ) {}

    @Get('all')
    syncAllData(): Promise<void> {
        return this.execService.syncAll();
    }

    @Get('cid')
    syncCidData(): Promise<void> {
        return this.cidService.syncData();
    }

    @Get('units')
    syncItemUnitData(): Promise<void> {
        return this.itemUnitService.syncData();
    }

    @Get('usage-vias')
    syncUsageViaData(): Promise<void> {
        return this.usageViaService.syncData();
    }

    @Get('substances')
    syncSubstanceData(): Promise<void> {
        return this.substanceService.syncData();
    }

    @Get('insurances')
    syncHealthInsuranceData(): Promise<void> {
        return this.healthInsuranceService.syncData();
    }

    @Get('patients')
    syncPatientData(): Promise<void> {
        return this.patientService.syncData();
    }

    @Get('allergies')
    syncAllergyData(): Promise<void> {
        return this.allergyService.syncData();
    }

    @Get('admissions')
    syncAdmissionData(): Promise<void> {
        return this.admissionService.syncData();
    }

    @Get('zones')
    syncAdmissionPrescriptionZoneData(): Promise<void> {
        return this.admissionPrescriptionZoneService.syncData();
    }

    @Get('boards')
    syncProfessionalBoardData(): Promise<void> {
        return this.professionalBoardService.syncData();
    }

    @Get('types')
    syncProfessionalTypeData(): Promise<void> {
        return this.professionalTypeService.syncData();
    }

    @Get('professionals')
    syncHealthcareProfessionalData(): Promise<void> {
        return this.healthcareProfessionalService.syncData();
    }

    @Get('prescriptions')
    syncAdmissionPrescriptionData(): Promise<void> {
        return this.admissionPrescriptionService.syncData();
    }

    @Get('prescription-items')
    syncPrescriptionItemData(): Promise<void> {
        return this.prescriptionItemService.syncData();
    }

    @Get('components')
    syncPrescriptionItemComponentData(): Promise<void> {
        return this.prescriptionItemComponentService.syncData();
    }
}

import { ProductModule } from './../prescription-items/product/product.module';
import { PrescriptionItemComponentModule } from './../prescription-items/prescription-item-component/prescription-item-component.module';
import { PrescriptionItemGroupModule } from './../prescription-items/prescription-item-group/prescription-item-group.module';
import { PrescriptionItemFrequencyModule } from './../prescription-items/prescription-item-frequency/prescription-item-frequency.module';
import { PrescriptionItemDescriptionModule } from './../prescription-items/prescription-item-description/prescription-item-description.module';
import { PrescriptionItemModule } from './../prescription-items/prescription-item/prescription-item.module';
import { AdmissionPrescriptionModule } from './../admission-prescription/admission-prescription.module';
import { HealthcareProfessionalModule } from './../backoffice/healthcare-professional/healthcare-professional.module';
import { ProfessionalTypeModule } from './../backoffice/professional-type/professional-type.module';
import { AdmissionPrescriptionZoneModule } from './../backoffice/admission-prescription-zone/admission-prescription-zone.module';
import { PatientAdmissionModule } from './../patient-admission/patient-admission.module';
import { NurseModule } from './../nurse/nurse.module';
import { HealthInsuranceModule } from './../health-insurance/health-insurance.module';
import { DoctorModule } from './../doctor/doctor.module';
import { CidModule } from './../cid/cid.module';
import { AdmissionHealthInsuranceModule } from './../admission-health-insurance/admission-health-insurance.module';
import { AdmissionCidModule } from './../admission-cid/admission-cid.module';
import { SubstanceModule } from './../substance/substance.module';
import { DatabaseModule } from './../../database/database.module';
import { PatientAllergyModule } from './../patient-allergy/patient-allergy.module';
import { PatientModule } from './../patient/patient.module';
import { ProfessionalBoardModule } from './../backoffice/professional-board/professional-board.module';
import { LoggerModule } from './../logger/logger.module';
import { ItemUnitModule } from '../prescription-items/item-unit/item-unit.module';
import { UsageViaModule } from '../prescription-items/usage-via/usage-via.module';

import { SyncSubstanceService } from './routine/sync-substance/sync-substance.service';
import { SyncAllergyService } from './routine/sync-allergy/sync-allergy.service';
import { SyncPatientService } from './routine/sync-patient/sync-patient.service';
import { SyncHealthInsuranceService } from './routine/sync-health-insurance/sync-health-insurance.service';
import { SyncCidService } from './provisioning/sync-cid/sync-cid.service';
import { SyncAdmissionService } from './routine/sync-admission/sync-admission.service';
import { SyncProfessionalBoardService } from './routine/sync-professional-board/sync-professional-board.service';
import { SyncAdmissionPrescriptionZoneService } from './routine/sync-admission-prescription-zone/sync-admission-prescription-zone.service';
import { SyncProfessionalTypeService } from './routine/sync-professional-type/sync-professional-type.service';
import { SyncHealthcareProfessionalService } from './routine/sync-healthcare-professional/sync-healthcare-professional.service';
import { SyncAdmissionPrescriptionService } from './routine/sync-admission-prescription/sync-admission-prescription.service';
import { SyncPrescriptionItemService } from './routine/sync-prescription-item/sync-prescription-item.service';
import { SyncPrescriptionItemComponentService } from './routine/sync-prescription-item-component/sync-prescription-item-component.service';
import { SyncItemUnitService } from './provisioning/sync-unit/sync-unit.service';
import { SyncUsageViaService } from './provisioning/sync-usage/sync-usage-via.service';

import { MvIntegrationController } from './mv-integration.controller';
import { ExecService } from './exec/exec.service';
import { UtilsService } from './utils/utils.service';

import { Module } from '@nestjs/common';

@Module({
  imports: [
    DatabaseModule,
    PatientModule,
    PatientAllergyModule,
    SubstanceModule,
    AdmissionCidModule,
    AdmissionHealthInsuranceModule,
    CidModule,
    DoctorModule,
    HealthInsuranceModule,
    NurseModule,
    PatientAdmissionModule,
    LoggerModule,
    ProfessionalBoardModule,
    AdmissionPrescriptionZoneModule,
    ProfessionalTypeModule,
    HealthcareProfessionalModule,
    AdmissionPrescriptionModule,
    PrescriptionItemModule,
    PrescriptionItemDescriptionModule,
    PrescriptionItemFrequencyModule,
    PrescriptionItemGroupModule,
    PrescriptionItemComponentModule,
    ProductModule,
    ItemUnitModule,
    UsageViaModule
  ],
  controllers: [
    MvIntegrationController
  ],
  providers: [
    SyncSubstanceService,
    SyncAllergyService,
    SyncPatientService,
    SyncHealthInsuranceService,
    SyncCidService,
    SyncAdmissionService,
    UtilsService,
    ExecService,
    SyncProfessionalBoardService,
    SyncAdmissionPrescriptionZoneService,
    SyncProfessionalTypeService,
    SyncHealthcareProfessionalService,
    SyncAdmissionPrescriptionService,
    SyncPrescriptionItemService,
    SyncPrescriptionItemComponentService,
    SyncItemUnitService,
    SyncUsageViaService
  ]
})
export class MvIntegrationModule {}

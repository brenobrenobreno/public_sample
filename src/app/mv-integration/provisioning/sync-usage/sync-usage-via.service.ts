import { Injectable, Inject } from '@nestjs/common';

import { UsageVia } from 'src/app/prescription-items/usage-via/usage-via.entity';

import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

import * as usageVia from '../../../../assets/usage_via.json';

@Injectable()
export class SyncUsageViaService {
    constructor(
        @Inject('USAGE_VIA_REPOSITORY') private usageViaProvider: typeof UsageVia,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncUsageViaService] Starting Usage Via data sync`);
        
        const MV_result = usageVia['via'];
        const formattedData = this.formatBulkUsageVia(MV_result);
        await this.bulkCreateUsageVia(formattedData);

        this.logger.log(`[SyncUsageViaService] Usage Via data sync complete`);
    }

    formatBulkUsageVia(usageVias: Array<{}>) {
        this.logger.log(`[SyncUsageViaService] Formatting ${usageVias.length} data objects...`);

        let usageViaArray: Array<{}> = [];

        usageVias.forEach((usageVia) => {
            const currentUsageVia = {
                id: uuidv4(),
                name: usageVia['name'] ? usageVia['name'] : 'NULL',
            };
            usageViaArray.push(currentUsageVia);
        });
        this.logger.log(`[SyncUsageViaService] ${usageViaArray.length} rows generated for entity 'UsageVia'`);
        return usageViaArray;
    }

    async bulkCreateUsageVia(usageViaArray: Array<{}>) {
        this.logger.log(`[SyncUsageViaService] Inserting ${usageViaArray.length} rows of data into 'usage_via' table...`);
        await this.usageViaProvider.bulkCreate(usageViaArray, { ignoreDuplicates: true }).catch((error) => {
            this.logger.log(`[SyncUsageViaService] An error occured: ${error}`);
        });
    }
}

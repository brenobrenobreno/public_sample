import { Injectable, Inject } from '@nestjs/common';

import { ItemUnit } from '../../../prescription-items/item-unit/item-unit.entity';
;
import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

import * as units from '../../../../assets/unit.json'

@Injectable()
export class SyncItemUnitService {
    constructor(
        @Inject('ITEM_UNIT_REPOSITORY') private itemUnitProvider: typeof ItemUnit,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncItemUnitService] Starting Item Unit data sync`);
        
        const MV_result = units['unit'];
        const formattedData = this.formatBulkItemUnit(MV_result);
        await this.bulkCreateItemUnit(formattedData);

        this.logger.log(`[SyncItemUnitService] Item Unit data sync complete`);
    }

    formatBulkItemUnit(units: Array<{}>) {
        this.logger.log(`[SyncItemUnitService] Formatting ${units.length} data objects...`);

        let unitsArray: Array<{}> = [];

        units.forEach((unit) => {
            const currentUnit = {
                id: uuidv4(),
                name: unit['name'] ? unit['name'] : 'NULL',
            };
            unitsArray.push(currentUnit);
        });
        this.logger.log(`[SyncItemUnitService] ${unitsArray.length} rows generated for entity 'ItemUnit'`);
        return unitsArray;
    }

    async bulkCreateItemUnit(unitArray: Array<{}>) {
        this.logger.log(`[SyncItemUnitService] Inserting ${unitArray.length} rows of data into 'item_unit' table...`);
        await this.itemUnitProvider.bulkCreate(unitArray, { ignoreDuplicates: true }).catch((error) => {
            this.logger.log(`[SyncItemUnitService] An error occured: ${error}`);
        });
    }
}

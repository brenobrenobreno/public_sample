import { Injectable, Inject } from '@nestjs/common';

import { Cid } from './../../../cid/cid.entity';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncCidService {
    constructor(
        @Inject('CID_REPOSITORY') private cidProvider: typeof Cid,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncCidService] Starting Cid data sync`);
        
        const MV_result = await this.utils.getDatafromAPI('/cid');
        const formattedData = this.formatBulkCid(MV_result);
        await this.bulkCreateCid(formattedData);

        this.logger.log(`[SyncCidService] Cid data sync complete`);
    }

    formatBulkCid(cidsFromMV: Array<{}>) {
        this.logger.log(`[SyncCidService] Formatting ${cidsFromMV.length} data objects...`);

        let cidArray: Array<{}> = [];

        cidsFromMV.forEach((cid) => {
            const currentCid = {
                id: uuidv4(),
                external_id: cid['CD_CID'] ? cid['CD_CID'] : null,
                cid_code: cid['CD_CID'] ? cid['CD_CID'] : null,
                cid_description: cid['DS_CID'] ? cid['DS_CID'] : null
            };
            cidArray.push(currentCid);
        });
        this.logger.log(`[SyncCidService] ${cidArray.length} rows generated for entity 'Cid'`);
        return cidArray;
    }

    async bulkCreateCid(cidArray: Array<{}>) {
        this.logger.log(`[SyncCidService] Inserting ${cidArray.length} rows of data into 'cid' table...`);
        await this.cidProvider.bulkCreate(cidArray, {
            updateOnDuplicate: [
                'external_id',
                'cid_code',
                'cid_description'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncCidService] An error occured: ${error}`);
        });
    }
}

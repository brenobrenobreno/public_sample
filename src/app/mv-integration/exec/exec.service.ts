import { SyncPrescriptionItemComponentService } from './../routine/sync-prescription-item-component/sync-prescription-item-component.service';
import { SyncPrescriptionItemService } from './../routine/sync-prescription-item/sync-prescription-item.service';
import { SyncAdmissionPrescriptionService } from './../routine/sync-admission-prescription/sync-admission-prescription.service';
import { SyncHealthcareProfessionalService } from './../routine/sync-healthcare-professional/sync-healthcare-professional.service';
import { SyncProfessionalTypeService } from './../routine/sync-professional-type/sync-professional-type.service';
import { SyncAdmissionPrescriptionZoneService } from '../routine/sync-admission-prescription-zone/sync-admission-prescription-zone.service';
import { SyncProfessionalBoardService } from './../routine/sync-professional-board/sync-professional-board.service';
import { SyncSubstanceService } from '../routine/sync-substance/sync-substance.service';
import { SyncAllergyService } from '../routine/sync-allergy/sync-allergy.service';
import { SyncPatientService } from '../routine/sync-patient/sync-patient.service';
import { SyncHealthInsuranceService } from '../routine/sync-health-insurance/sync-health-insurance.service';
import { SyncCidService } from '../provisioning/sync-cid/sync-cid.service';
import { SyncAdmissionService } from '../routine/sync-admission/sync-admission.service';
import { SyncItemUnitService } from '../provisioning/sync-unit/sync-unit.service';
import { SyncUsageViaService } from '../provisioning/sync-usage/sync-usage-via.service';

import { Injectable } from '@nestjs/common';
import { LoggerService } from '../../logger/logger.service';

@Injectable()
export class ExecService {
    constructor(
        private readonly cidService: SyncCidService,
        private readonly substanceService: SyncSubstanceService,
        private readonly itemUnitService: SyncItemUnitService,
        private readonly usageViaService: SyncUsageViaService,
        private readonly healthInsuranceService: SyncHealthInsuranceService,
        private readonly patientService: SyncPatientService,
        private readonly allergyService: SyncAllergyService,
        private readonly admissionService: SyncAdmissionService,
        private readonly professionalBoardService: SyncProfessionalBoardService,
        private readonly admissionPrescriptionZoneService: SyncAdmissionPrescriptionZoneService,
        private readonly professionalTypeService: SyncProfessionalTypeService,
        private readonly healthcareProfessionalService: SyncHealthcareProfessionalService,
        private readonly admissionPrescriptionService: SyncAdmissionPrescriptionService,
        private readonly prescriptionItemService: SyncPrescriptionItemService,
        private readonly prescriptionItemComponentService: SyncPrescriptionItemComponentService,
        private readonly logger: LoggerService
    ) {}

    async syncAll() {
        this.logger.log(`[ExecService] Starting data sync from remote database`);

        await this.cidService.syncData();
        await this.itemUnitService.syncData();
        await this.usageViaService.syncData();
        await this.substanceService.syncData();
        await this.healthInsuranceService.syncData();
        await this.patientService.syncData();
        await this.allergyService.syncData();
        await this.admissionService.syncData();
        await this.professionalBoardService.syncData();
        await this.admissionPrescriptionZoneService.syncData();
        await this.professionalTypeService.syncData();
        await this.healthcareProfessionalService.syncData();
        await this.admissionPrescriptionService.syncData();
        await this.prescriptionItemService.syncData();
        await this.prescriptionItemComponentService.syncData();

        this.logger.log(`[ExecService] Data sync complete`);
    }
}

import { Injectable, Inject } from '@nestjs/common';

import { ContactInfo } from './../../../contact-info/contact-info.entity';
import { Person } from './../../../person/person.entity';
import { HealthcareProfessional } from './../../../backoffice/healthcare-professional/healthcare-professional.entity';
import { ProfessionalType } from './../../../backoffice/professional-type/professional-type.entity';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { MaritalStatus } from './../../../shared/enum/marital-status';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncHealthcareProfessionalService {
    constructor(
        @Inject('PERSONS_REPOSITORY') private personsProvider: typeof Person,
        @Inject('HEALTHCARE_PROFESSIONAL_REPOSITORY') private healthcareProfessionalProvider: typeof HealthcareProfessional,
        @Inject('CONTACT_INFO_REPOSITORY') private contactInfoProvider: typeof ContactInfo,
        @Inject('PROFESSIONAL_TYPE_REPOSITORY') private professionalTypeProvider: typeof ProfessionalType,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncHealthcareProfessionalService] Starting HealthcareProfessional data sync`);

        const MV_result = await this.utils.getDatafromAPI('/prestadores');
        const formattedData = await this.formatBulkHealthcareProfessional(MV_result);
        await this.bulkCreateHealthcareProfessional(formattedData);

        this.logger.log(`[SyncHealthcareProfessionalService] HealthcareProfessional data sync complete`);
    }

    async formatBulkHealthcareProfessional(professionalsFromMV: Array<{}>) {
        this.logger.log(`[SyncHealthcareProfessionalService] Formatting ${professionalsFromMV.length} data objects for Provider...`);

        const professionalTypeTable = await this.professionalTypeProvider.findAll();

        let contactInfoArray: Array<{}> = [];
        let personArray: Array<{}> = [];
        let professionalArray: Array<{}> = [];

        professionalsFromMV.forEach((professional) => {
            const contactInfoId = uuidv4();
            const personId = uuidv4();
            const professionalId = uuidv4();

            const currentContactInfo = {
                id: contactInfoId,
                external_id: null,
                email: professional['DS_EMAIL'] ? professional['DS_EMAIL'] : null,
                landline_number: professional['NR_FONE_CONTATO'] ? professional['NR_FONE_CONTATO'] : null,
                cellphone_number: professional['NR_FONE_CONTATO'] ? professional['NR_FONE_CONTATO'] : null,
                social_media_chat: null
            };
            const currentPerson = {
                id: personId,
                external_id: null,
                photo_id_code: null,
                name: professional['NM_PRESTADOR'] ? professional['NM_PRESTADOR'] : '',
                social_name: null,
                gender: 'U',
                birthday: professional['DT_NASCIMENTO'] ? this.utils.formatDatetimeToDate(professional['DT_NASCIMENTO']) : '1990-01-01',
                marital_status: MaritalStatus.unknown,
                social_security_number: professional['NR_DOCUMENTO'] ? professional['NR_DOCUMENTO'] : '',
                alternative_social_security_number: professional['NR_CPF_CGC'] ? professional['NR_CPF_CGC'] : '',
                zip_code: professional['NR_CEP'] ? professional['NR_CEP'] : '',
                street: professional['DS_ENDERECO'] ? professional['DS_ENDERECO'] + " " + professional['NR_ENDERECO'] : null,
                district: professional['DS_BAIRRO'] ? professional['DS_BAIRRO'] : '',
                city: professional['NM_CIDADE'] ? professional['NM_CIDADE'] : '',
                state: professional['CD_UF'] ? professional['CD_UF'] : '',
                address_complement: professional['DS_COMPLEMENTO'] ? professional['DS_COMPLEMENTO'] : '',
                contact_info_id: contactInfoId
            };
            const currentProfessional = {
                id: professionalId,
                external_id: professional['CD_PRESTADOR'] ? professional['CD_PRESTADOR'] : null,
                professional_type_id: professional['CD_TIP_PRESTA'] ? professionalTypeTable.find(type => type.external_id == professional['CD_TIP_PRESTA']).id : '',
                board_code: professional['DS_CODIGO_CONSELHO'] ? professional['DS_CODIGO_CONSELHO'] : null,
                person_id: personId
            };
            contactInfoArray.push(currentContactInfo);
            personArray.push(currentPerson);
            professionalArray.push(currentProfessional);
        })
        this.logger.log(`[SyncHealthcareProfessionalService] ${contactInfoArray.length} Provider rows generated for entity 'ContactInfo'`);
        this.logger.log(`[SyncHealthcareProfessionalService] ${personArray.length} Provider rows generated for entity 'Person'`);
        this.logger.log(`[SyncHealthcareProfessionalService] ${professionalArray.length} Provider rows generated for entity 'HealthcareProfessional'`);
        return [contactInfoArray, personArray, professionalArray];
    }

    async bulkCreateHealthcareProfessional(professionalArray: Array<object[]>) {
        try {
            this.logger.log(`[SyncHealthcareProfessionalService] Inserting ${professionalArray[0].length} rows of data into 'contact_info' table...`);
            await this.contactInfoProvider.bulkCreate(professionalArray[0], {
                updateOnDuplicate: [
                    'email',
                    'landline_number',
                    'cellphone_number',
                    'social_media_chat'
                ]
            });
            this.logger.log(`[SyncHealthcareProfessionalService] Inserting ${professionalArray[1].length} rows of data into 'person' table...`);
            await this.personsProvider.bulkCreate(professionalArray[1], {
                updateOnDuplicate: [
                    'name',
                    'social_name',
                    'gender',
                    'birthday',
                    'marital_status',
                    'social_security_number',
                    'alternative_social_security_number',
                    'zip_code',
                    'street',
                    'district',
                    'city',
                    'state',
                    'address_complement'
                ]
            });
            this.logger.log(`[SyncHealthcareProfessionalService] Inserting ${professionalArray[2].length} rows of data into 'healthcare_professional' table...`);
            await this.healthcareProfessionalProvider.bulkCreate(professionalArray[2], {
                updateOnDuplicate: [
                    'board_code'
                ]
            });
        } catch (error) {
            this.logger.log(`[SyncHealthcareProfessionalService] An error occured: ${error}`);
        }
    }
}

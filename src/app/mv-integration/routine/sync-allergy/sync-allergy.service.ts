import { Injectable, Inject } from '@nestjs/common';

import { Substance } from './../../../substance/substance.entity';
import { Patient } from './../../../patient/patient.entity';
import { PatientAllergy } from './../../../patient-allergy/patient-allergy.entity';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncAllergyService {
    constructor(
        @Inject('PATIENT_ALLERGIES_REPOSITORY') private patientAllergyProvider: typeof PatientAllergy,
        @Inject('PATIENTS_REPOSITORY') private patientsProvider: typeof Patient,
        @Inject('SUBSTANCES_REPOSITORY') private substancesProvider: typeof Substance,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncAllergyService] Starting Allergy data sync`);

        const MV_result = await this.utils.getDatafromAPI('/alergias');
        const formattedData = await this.formatBulkAllergies(MV_result);
        await this.bulkCreateAllergies(formattedData);

        this.logger.log(`[SyncAllergyService] Allergy data sync complete`);
    }

    async formatBulkAllergies(allergiesFromMV: Array<{}>) {
        this.logger.log(`[SyncAllergyService] Formatting ${allergiesFromMV.length} data objects...`);

        const patientTable = await this.patientsProvider.findAll();
        const substanceTable = await this.substancesProvider.findAll();

        var allergiesArray: Array<{}> = [];

        allergiesFromMV.forEach((allergy) => {
            const currentPatientAllergy = {
                id: uuidv4(),
                external_id: null,
                substance_id: allergy['CD_SUBSTANCIA'] ? substanceTable.find(substance => substance.external_id == allergy['CD_SUBSTANCIA']).id : '',
                status: null,
                description: allergy['DS_AVISO'] ? allergy['DS_AVISO'] : '',
                severity: allergy['TP_SEVERIDADE'] ? allergy['TP_SEVERIDADE'] : '',
                patient_id: allergy['CD_PACIENTE'] ? patientTable.find(patient => patient.external_id == allergy['CD_PACIENTE']).id : ''
            }
            allergiesArray.push(currentPatientAllergy);
        });
        this.logger.log(`[SyncAllergyService] ${allergiesArray.length} rows generated for entity 'PatientAllergy'`);
        return allergiesArray;
    }

    async bulkCreateAllergies(allergyArray: Array<{}>) {
        this.logger.log(`[SyncAllergyService] Inserting ${allergyArray.length} rows of data into 'patient_allergy' table...`);
        await this.patientAllergyProvider.bulkCreate(allergyArray, {
            updateOnDuplicate: [
                'status',
                'description',
                'severity'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncAllergyService] An error occured: ${error}`);
        });
    }
}

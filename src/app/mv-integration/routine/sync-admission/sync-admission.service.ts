import { Inject, Injectable } from '@nestjs/common';

import { Patient } from './../../../patient/patient.entity';
import { AdmissionHealthInsurance } from './../../../admission-health-insurance/admission-health-insurance.entity';
import { AdmissionCid } from './../../../admission-cid/admission-cid.entity';
import { HealthInsurance } from './../../../health-insurance/health-insurance.entity';
import { Cid } from './../../../cid/cid.entity';
import { PatientAdmission } from './../../../patient-admission/patient-admission.entity';

import { AdmissionState } from './../../../shared/enum/admission-state';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncAdmissionService {
    constructor(
        @Inject('CID_REPOSITORY') private cidProvider: typeof Cid,
        @Inject('HEALTH_INSURANCE_REPOSITORY') private healthInsuranceProvider: typeof HealthInsurance,
        @Inject('PATIENTS_REPOSITORY') private patientsProvider: typeof Patient,
        @Inject('PATIENT_ADMISSION_REPOSITORY') private patientAdmissionProvider: typeof PatientAdmission,
        @Inject('ADMISSION_CID_REPOSITORY') private admissionCidProvider: typeof AdmissionCid,
        @Inject('ADMISSION_HEALTH_INSURANCE_REPOSITORY') private admissionHealthInsuranceProvider: typeof AdmissionHealthInsurance,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncAdmissionService] Starting Admission data sync`);

        const MV_result = await this.utils.getDatafromAPI('/atendimentos');
        const patientAdmissionFormattedData = await this.formatBulkAdmissions(MV_result);
        await this.bulkCreateAdmissions(patientAdmissionFormattedData);

        this.logger.log(`[SyncAdmissionService] Admission data sync complete`);
    }

    async formatBulkAdmissions(admissionsFromMV: Array<{}>) {
        this.logger.log(`[SyncAdmissionService] Formatting ${admissionsFromMV.length} data objects...`);

        const patientTable = await this.patientsProvider.findAll();
        const cidTable = await this.cidProvider.findAll();
        const healthInsuranceTable = await this.healthInsuranceProvider.findAll();

        let patientAdmissionArray: Array<{}> = [];
        let admissionCidArray: Array<{}> = [];
        let admissionHealthInsuranceArray: Array<{}> = [];

        admissionsFromMV.forEach((admission) => {
            const patientAdmissionId = uuidv4();
            const admissionHealthInsuranceId = uuidv4();
            const admissionCidId = uuidv4();

            const currentAdmission = {
                id: patientAdmissionId,
                external_id: admission['CD_ATENDIMENTO'] ? admission['CD_ATENDIMENTO'] : null,
                patient_admission_datetime: admission['DT_ATENDIMENTO'] ? (this.utils.formatDate(admission['DT_ATENDIMENTO']) + " " + admission['HR_ATENDIMENTO']) : null,
                patient_admission_state: AdmissionState.active,
                palliative_care: null,
                public_health_insurance_register: null,
                confidential_medical_record: null,
                long_term: null,
                patient_id: admission['CD_PACIENTE'] ? patientTable.find(patient => patient.external_id == admission['CD_PACIENTE']).id : ''
            };
            const currentAdmissionHealthInsurance = {
                id: admissionHealthInsuranceId,
                external_id: null,
                health_insurance_holder: null,
                health_insurance_validity: null,
                patient_health_insurance_register: null,
                patient_admission_id: patientAdmissionId,
                health_insurance_id: admission['CD_CONVENIO'] ? healthInsuranceTable.find(healthInsurance => healthInsurance.external_id == admission['CD_CONVENIO']).id : ''
            };
            const currentAdmissionCid = {
                id: admissionCidId,
                external_id: null,
                cid_id: admission['CD_CID'] ? cidTable.find(cid => cid.cid_code == admission['CD_CID']).id : '',
                admission_cid_state: AdmissionState.active,
                patient_admission_id: patientAdmissionId
            };

            admissionCidArray.push(currentAdmissionCid);
            admissionHealthInsuranceArray.push(currentAdmissionHealthInsurance);
            patientAdmissionArray.push(currentAdmission);
        })
        this.logger.log(`[SyncAdmissionService] ${patientAdmissionArray.length} rows generated for entity 'PatientAdmission'`);
        this.logger.log(`[SyncAdmissionService] ${admissionHealthInsuranceArray.length} rows generated for entity 'AdmissionHealthInsurance'`);
        this.logger.log(`[SyncAdmissionService] ${admissionCidArray.length} rows generated for entity 'AdmissionCid'`);
        return [patientAdmissionArray, admissionHealthInsuranceArray, admissionCidArray];
    }

    async bulkCreateAdmissions(admissionsArray: Array<object[]>) {
        this.logger.log(`[SyncAdmissionService] Inserting ${admissionsArray[0].length} rows of data into 'patient_admission' table...`);
        try {
            await this.patientAdmissionProvider.bulkCreate(admissionsArray[0], {
                updateOnDuplicate: [
                    'patient_admission_datetime',
                    'patient_admission_state',
                    'doctor_id',
                    'supervising_nurse_id',
                    'palliative_care',
                    'public_health_insurance_register',
                    'confidential_medical_record',
                    'long_term'
                ]
            });
            this.logger.log(`[SyncAdmissionService] Inserting ${admissionsArray[1].length} rows of data into 'admission_health_insurance' table...`);
            await this.admissionHealthInsuranceProvider.bulkCreate(admissionsArray[1], {
                updateOnDuplicate: [
                    'health_insurance_holder',
                    'health_insurance_validity',
                    'patient_health_insurance_register'
                ]
            });
            this.logger.log(`[SyncAdmissionService] Inserting ${admissionsArray[2].length} rows of data into 'admission_cid' table...`);
            await this.admissionCidProvider.bulkCreate(admissionsArray[2], {
                updateOnDuplicate: [
                    'admission_cid_state'
                ]
            });
        } catch (error) {
            this.logger.log(`[SyncAdmissionService] An error occured: ${error}`);
        }
    }
}

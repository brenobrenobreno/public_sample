import { ProfessionalType } from './../../../backoffice/professional-type/professional-type.entity';
import { ProfessionalBoard } from './../../../backoffice/professional-board/professional-board.entity';

import { Injectable, Inject } from '@nestjs/common';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncProfessionalTypeService {
    constructor(
        @Inject('PROFESSIONAL_BOARD_REPOSITORY') private professionalBoardProvider: typeof ProfessionalBoard,
        @Inject('PROFESSIONAL_TYPE_REPOSITORY') private professionalTypeProvider: typeof ProfessionalType,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncProfessionalTypeService] Starting ProfessionalType data sync`);
        
        const MV_result = await this.utils.getDatafromAPI('/prestadores');
        const formattedData = await this.formatBulkProfessionalType(MV_result);
        await this.bulkCreateProfessionalType(formattedData);

        this.logger.log(`[SyncProfessionalTypeService] ProfessionalType data sync complete`);
    }

    async formatBulkProfessionalType(prescriptionsFromMV: Array<{}>) {
        this.logger.log(`[SyncProfessionalTypeService] Formatting ${prescriptionsFromMV.length} data objects...`);

        const professionalBoardTable = await this.professionalBoardProvider.findAll();

        let typeArray: Array<{}> = [];

        prescriptionsFromMV.forEach((prescription) => {
            const currentType = {
                id: uuidv4(),
                external_id: prescription['CD_TIP_PRESTA'] ? prescription['CD_TIP_PRESTA'] : null,
                name: prescription['TIPO_PRESTADOR'] ? prescription['TIPO_PRESTADOR'] : '',
                professional_board_id: prescription['COD_CONSELHO'] ? professionalBoardTable.find(board => board.external_id == prescription['COD_CONSELHO']).id : ''
            };
            typeArray.push(currentType);
        });
        this.logger.log(`[SyncProfessionalTypeService] ${typeArray.length} rows generated for entity 'ProfessionalType'`);
        return typeArray;
    }

    async bulkCreateProfessionalType(typeArray: Array<{}>) {
        this.logger.log(`[SyncProfessionalTypeService] Inserting ${typeArray.length} rows of data into 'professional_type' table...`);
        await this.professionalTypeProvider.bulkCreate(typeArray, {
            updateOnDuplicate: [
                'name'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncProfessionalTypeService] An error occured: ${error}`);
        });
    }
}

import { PatientAdmission } from './../../../patient-admission/patient-admission.entity';
import { AdmissionPrescriptionZone } from './../../../backoffice/admission-prescription-zone/admission-prescription-zone.entity';
import { AdmissionPrescription } from './../../../admission-prescription/admission-prescription.entity';
import { HealthcareProfessional } from './../../../backoffice/healthcare-professional/healthcare-professional.entity';
import { Injectable, Inject } from '@nestjs/common';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncAdmissionPrescriptionService {
    constructor(
        @Inject('ADMISSION_PRESCRIPTION_REPOSITORY') private admissionPrescriptionProvider: typeof AdmissionPrescription,
        @Inject('ADMISSION_PRESCRIPTION_ZONE_REPOSITORY') private admissionPrescriptionZoneProvider: typeof AdmissionPrescriptionZone,
        @Inject('PATIENT_ADMISSION_REPOSITORY') private patientAdmissionProvider: typeof PatientAdmission,
        @Inject('HEALTHCARE_PROFESSIONAL_REPOSITORY') private healthcareProfessionalProvider: typeof HealthcareProfessional,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncAdmissionPrescriptionService] Starting AdmissionPrescription data sync`);
        
        const MV_result = await this.utils.getDatafromAPI('/prescricoes');
        const formattedData = await this.formatBulkAdmissionPrescription(MV_result);
        await this.bulkCreateAdmissionPrescription(formattedData);

        this.logger.log(`[SyncAdmissionPrescriptionService] AdmissionPrescription data sync complete`);
    }

    async formatBulkAdmissionPrescription(prescriptionsFromMV: Array<{}>) {
        this.logger.log(`[SyncAdmissionPrescriptionService] Formatting ${prescriptionsFromMV.length} data objects...`);

        const unitTable = await this.admissionPrescriptionZoneProvider.findAll();
        const admissionTable = await this.patientAdmissionProvider.findAll();
        const professionalTable = await this.healthcareProfessionalProvider.findAll();

        let admissionPrescriptionArray: Array<{}> = [];

        prescriptionsFromMV.forEach((prescription) => {
            const currentAdmissionPrescription = {
                id: uuidv4(),
                external_id: prescription['CD_PRE_MED'] ? prescription['CD_PRE_MED'] : null,
                admission_prescription_cancelled: prescription['SN_FECHADO'] ? this.utils.formatPrescriptionCancelledStatus(prescription['SN_FECHADO']) : null,
                initial_datetime: prescription['HR_PRE_MED'] ? (this.utils.formatDate(prescription['DT_PRE_MED']) + " " + prescription['HR_PRE_MED']) : prescription['DT_PRE_MED'],
                final_datetime: prescription['DT_VALIDADE'] ? this.utils.formatDate(prescription['DT_VALIDADE']) : null,
                admission_prescription_zone_id: prescription['CD_UNID_INT'] ? unitTable.find(unit => unit.external_id == prescription['CD_UNID_INT']).id : null,
                patient_admission_id: prescription['CD_ATENDIMENTO'] ? admissionTable.find(admission => admission.external_id == prescription['CD_ATENDIMENTO']).id : null,
                prescription_provider_id: prescription['COD_PRESTADOR'] ? professionalTable.find(provider => provider.external_id == prescription['COD_PRESTADOR']).id : null,
                prescription_transcriber_id: prescription['COD_TRANSCRITOR'] ? professionalTable.find(transcriptor => transcriptor.external_id == prescription['COD_TRANSCRITOR']).id : null
            };
            admissionPrescriptionArray.push(currentAdmissionPrescription);
        });

        this.logger.log(`[SyncAdmissionPrescriptionService] ${admissionPrescriptionArray.length} rows generated for entity 'AdmissionPrescription'`);
        return admissionPrescriptionArray;
    }

    async bulkCreateAdmissionPrescription(admissionPrescriptionArray: Array<{}>) {
        this.logger.log(`[SyncAdmissionPrescriptionService] Inserting ${admissionPrescriptionArray.length} rows of data into 'admission_prescription' table...`);
        await this.admissionPrescriptionProvider.bulkCreate(admissionPrescriptionArray, {
            updateOnDuplicate: [
                'admission_prescription_cancelled',
                'initial_datetime',
                'final_datetime'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncAdmissionPrescriptionService] An error occured: ${error}`);
        });
    }
}

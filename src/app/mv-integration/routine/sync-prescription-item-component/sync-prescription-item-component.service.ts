import { PrescriptionItemComponent } from './../../../prescription-items/prescription-item-component/prescription-item-component.entity';
import { PrescriptionItem } from './../../../prescription-items/prescription-item/prescription-item.entity';
import { Product } from './../../../prescription-items/product/product.entity';
import { Injectable, Inject } from '@nestjs/common';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncPrescriptionItemComponentService {
    constructor(
        @Inject('PRODUCT_REPOSITORY') private productProvider: typeof Product,
        @Inject('PRESCRIPTION_ITEM_REPOSITORY') private prescriptionItemProvider: typeof PrescriptionItem,
        @Inject('PRESCRIPTION_ITEM_COMPONENT_REPOSITORY') private prescriptionItemComponentProvider: typeof PrescriptionItemComponent,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncPrescriptionItemComponentService] Starting Product data sync`);
        const MV_result = await this.utils.getDatafromAPI('/componentes');

        const formattedDataProducts = this.formatBulkProducts(MV_result);
        await this.bulkCreateProducts(formattedDataProducts);
        this.logger.log(`[SyncPrescriptionItemComponentService] Product data sync complete`);

        this.logger.log(`[SyncPrescriptionItemComponentService] Starting Prescription Item Component data sync`);
        const formattedDataComponents = await this.formatBulkComponents(MV_result);
        await this.bulkCreateComponents(formattedDataComponents);
        this.logger.log(`[SyncPrescriptionItemComponentService] Prescription Item Component data sync complete`);
    }

    formatBulkProducts(componentsFromMV: Array<{}>) {
        this.logger.log(`[SyncProductService] Formatting ${componentsFromMV.length} data objects...`);

        var productsArray: Array<{}> = [];
        
        componentsFromMV.forEach((component) => {
            const currentProduct = {
                id: uuidv4(),
                external_id: component['CD_PRODUTO'] ? component['CD_PRODUTO'] : null,
                name: component['DS_PRODUTO'] ? component['DS_PRODUTO'] : 'NULL',
                unit: component['DS_UNIDADE'] ? component['DS_UNIDADE'] : null
            }
            productsArray.push(currentProduct);
        })
        this.logger.log(`[SyncProductService] ${productsArray.length} rows generated for entity 'Product'`);
        return productsArray;
    }

    async bulkCreateProducts(productsArray: Array<{}>) {
        this.logger.log(`[SyncProductService] Inserting ${productsArray.length} rows of data into 'product' table...`);

        let start = 0;
        let end = productsArray.length;
        let half = Math.round(productsArray.length/2);

        let arrayPart1 = productsArray.slice(start, half);
        let arrayPart2 = productsArray.slice(half, end);

        await this.productProvider.bulkCreate(arrayPart1, {
            updateOnDuplicate: [
                'name',
                'unit'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncProductService] An error occured: ${error}`);
        });
        await this.productProvider.bulkCreate(arrayPart2, {
            updateOnDuplicate: [
                'name',
                'unit'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncProductService] An error occured: ${error}`);
        });
    }

    async formatBulkComponents(componentsFromMV: Array<{}>) {
        this.logger.log(`[SyncPrescriptionItemComponentService] Formatting ${componentsFromMV.length} data objects...`);

        const productTable = await this.productProvider.findAll();
        const prescriptionItemTable = await this.prescriptionItemProvider.findAll();

        var componentsArray: Array<{}> = [];

        componentsFromMV.forEach((component) => {
            const currentComponent = {
                id: uuidv4(),
                external_id: component['CD_CITPRE_MED'] ? component['CD_CITPRE_MED'] : null,
                product_id: component['CD_PRODUTO'] ? productTable.find(product => product.external_id == component['CD_PRODUTO']).id : null,
                quantity: component['QT_COMPONENTE'] ? component['QT_COMPONENTE'] : null,
                prescription_item_id: component['CD_ITPRE_MED'] ? prescriptionItemTable.find(item => item.external_id == component['CD_ITPRE_MED']).id : null,
                comments: component['DS_CITPRE_MED'] ? component['DS_CITPRE_MED'] : null
            }
            componentsArray.push(currentComponent);
        });
        this.logger.log(`[SyncPrescriptionItemComponentService] ${componentsArray.length} rows generated for entity 'PrescriptionItemComponent'`);
        return componentsArray;
    }

    async bulkCreateComponents(componentsArray: Array<{}>) {
        this.logger.log(`[SyncPrescriptionItemComponentService] Inserting ${componentsArray.length} rows of data into 'prescription_item_component' table...`);

        let start = 0;
        let end = componentsArray.length;
        let half = Math.round(componentsArray.length/2);

        let arrayPart1 = componentsArray.slice(start, half);
        let arrayPart2 = componentsArray.slice(half, end);

        await this.prescriptionItemComponentProvider.bulkCreate(arrayPart1, {
            updateOnDuplicate: [
                'quantity',
                'comments'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncPrescriptionItemComponentService] An error occured: ${error}`);
        });
        await this.prescriptionItemComponentProvider.bulkCreate(arrayPart2, {
            updateOnDuplicate: [
                'quantity',
                'comments'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncPrescriptionItemComponentService] An error occured: ${error}`);
        });
    }
}

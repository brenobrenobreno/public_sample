import { Injectable, Inject } from '@nestjs/common';

import { Substance } from './../../../substance/substance.entity';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncSubstanceService {
    constructor(
        @Inject('SUBSTANCES_REPOSITORY') private substanceProvider: typeof Substance,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncSubstanceService] Starting Substance data sync`);

        const MV_result = await this.utils.getDatafromAPI('/substancias');
        const formattedData = this.formatBulkSubstances(MV_result);
        await this.bulkCreateSubstances(formattedData);

        this.logger.log(`[SyncSubstanceService] Substance data sync complete`);
    }
    
    formatBulkSubstances(substancesFromMV: Array<{}>) {
        this.logger.log(`[SyncSubstanceService] Formatting ${substancesFromMV.length} data objects...`);

        var substancesArray: Array<{}> = [];
        
        substancesFromMV.forEach((substance) => {
            const currentSubstance = {
                id: uuidv4(),
                external_id: substance['CD_SUBSTANCIA'] ? substance['CD_SUBSTANCIA'] : null,
                substance: substance['DS_SUBSTANCIA'] ? substance['DS_SUBSTANCIA'] : null,
                category: null
            }
            substancesArray.push(currentSubstance);
        })
        this.logger.log(`[SyncSubstanceService] ${substancesArray.length} rows generated for entity 'Substance'`);
        return substancesArray;
    }

    async bulkCreateSubstances(substanceArray: Array<{}>) {
        this.logger.log(`[SyncSubstanceService] Inserting ${substanceArray.length} rows of data into 'substance' table...`);
        await this.substanceProvider.bulkCreate(substanceArray, {
            updateOnDuplicate: [
                'substance',
                'category'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncSubstanceService] An error occured: ${error}`);
        });
    }
}

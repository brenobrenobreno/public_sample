import { Person } from './../../../person/person.entity';
import { HealthcareProfessional } from './../../../backoffice/healthcare-professional/healthcare-professional.entity';
import { AdmissionPrescription } from './../../../admission-prescription/admission-prescription.entity';
import { PrescriptionItemGroup } from './../../../prescription-items/prescription-item-group/prescription-item-group.entity';
import { PrescriptionItemFrequency } from './../../../prescription-items/prescription-item-frequency/prescription-item-frequency.entity';
import { PrescriptionItemDescription } from './../../../prescription-items/prescription-item-description/prescription-item-description.entity';
import { PrescriptionItem } from './../../../prescription-items/prescription-item/prescription-item.entity';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { Injectable, Inject } from '@nestjs/common';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncPrescriptionItemService {
    constructor(
        @Inject('PRESCRIPTION_ITEM_REPOSITORY') private prescriptionItemProvider: typeof PrescriptionItem,
        @Inject('PRESCRIPTION_ITEM_DESCRIPTION_REPOSITORY') private prescriptionItemDescriptionProvider: typeof PrescriptionItemDescription,
        @Inject('PRESCRIPTION_ITEM_GROUP_REPOSITORY') private prescriptionItemGroupProvider: typeof PrescriptionItemGroup,
        @Inject('PRESCRIPTION_ITEM_FREQUENCY_REPOSITORY') private prescriptionItemFrequencyProvider: typeof PrescriptionItemFrequency,
        @Inject('ADMISSION_PRESCRIPTION_REPOSITORY') private admissionPrescriptionProvider: typeof AdmissionPrescription,
        @Inject('HEALTHCARE_PROFESSIONAL_REPOSITORY') private healthcareProfessionalProvider: typeof HealthcareProfessional,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncPrescriptionItemService] Starting Prescription Item data sync`);

        const MV_result = await this.utils.getDatafromAPI('/itens-prescricao');

        const formattedDataForAuxTables = this.formatBulkPrescriptionItemsAux(MV_result);
        await this.bulkCreatePrescriptionItemsAux(formattedDataForAuxTables);

        const formattedDataForMainTable = await this.formatBulkPrescriptionItems(MV_result);
        await this.bulkCreatePrescriptionItems(formattedDataForMainTable);

        this.logger.log(`[SyncPrescriptionItemService] Prescription Item data sync complete`);
    }

    formatBulkPrescriptionItemsAux(prescriptionItemsFromMV) {
        this.logger.log(`[SyncPrescriptionItemService] Formatting ${prescriptionItemsFromMV.length} data objects...`);

        let prescriptionItemDescriptionArray: Array<{}> = [];
        let prescriptionItemGroupArray: Array<{}> = [];
        let prescriptionItemFrequencyArray: Array<{}> = [];

        prescriptionItemsFromMV.forEach((prescriptionItem) => {
            const currentPrescriptionItemDescription = {
                id: uuidv4(),
                external_id: prescriptionItem['CD_TIP_PRESC'] ? prescriptionItem['CD_TIP_PRESC'] : null,
                description: prescriptionItem['DS_TIP_PRESC'] ? prescriptionItem['DS_TIP_PRESC'] : null
            }

            const currentPrescriptionItemFrequency = {
                id: uuidv4(),
                external_id: prescriptionItem['CD_TIP_FRE'] ? prescriptionItem['CD_TIP_FRE'] : null,
                frequency: prescriptionItem['DS_TIP_FRE'] ? prescriptionItem['DS_TIP_FRE'] : null,
                fixed: prescriptionItem['FREQUENCIA_FIXA'] ? this.utils.formatFrequencyFix(prescriptionItem['FREQUENCIA_FIXA']) : false,
                value: prescriptionItem['NR_INTERVALO'] ? prescriptionItem['NR_INTERVALO'] : null,
                type: prescriptionItem['TP_INTERVALO'] ? prescriptionItem['TP_INTERVALO'] : null
            }
    
            const currentPrescriptionItemGroup = {
                id: uuidv4(),
                external_id: prescriptionItem['CD_TIP_ESQ'] ? prescriptionItem['CD_TIP_ESQ'] : null,
                name: prescriptionItem['DS_TIP_ESQ'] ? prescriptionItem['DS_TIP_ESQ'] : null
            }
    
            prescriptionItemFrequencyArray.push(currentPrescriptionItemFrequency);
            prescriptionItemGroupArray.push(currentPrescriptionItemGroup);
            prescriptionItemDescriptionArray.push(currentPrescriptionItemDescription);
        });
        this.logger.log(`[SyncPrescriptionItemService] ${prescriptionItemFrequencyArray.length} rows generated for entity 'PrescriptionItemFrequency'`);
        this.logger.log(`[SyncPrescriptionItemService] ${prescriptionItemGroupArray.length} rows generated for entity 'PrescriptionItemGroup'`);
        this.logger.log(`[SyncPrescriptionItemService] ${prescriptionItemDescriptionArray.length} rows generated for entity 'PrescriptionItemDescription'`);

        return [prescriptionItemFrequencyArray, prescriptionItemGroupArray, prescriptionItemDescriptionArray]
    }

    async formatBulkPrescriptionItems(prescriptionItemsFromMV) {
        this.logger.log(`[SyncPrescriptionItemService] Formatting ${prescriptionItemsFromMV.length} data objects...`);
        let prescriptionItemArray: Array<{}> = [];

        const frequencyTable = await this.prescriptionItemFrequencyProvider.findAll();
        const groupTable = await this.prescriptionItemGroupProvider.findAll();
        const prescriptionTable = await this.admissionPrescriptionProvider.findAll();
        const descriptionTable = await this.prescriptionItemDescriptionProvider.findAll();
        const professionalTable = await this.healthcareProfessionalProvider.findAll({
            include: [{ model: Person }]
        });

        prescriptionItemsFromMV.forEach(prescriptionItem => {
            const currentPrescriptionItem = {
                id: uuidv4(),
                external_id: prescriptionItem['CD_ITPRE_MED'] ? prescriptionItem['CD_ITPRE_MED'] : null,
                prescription_item_group_id: prescriptionItem['CD_TIP_ESQ'] ? groupTable.find(group => group.external_id == prescriptionItem['CD_TIP_ESQ']).id : null,
                prescription_item_description_id: prescriptionItem['CD_TIP_PRESC'] ? descriptionTable.find(description => description.external_id == prescriptionItem['CD_TIP_PRESC']).id : null,
                quantity: prescriptionItem['QT_ITPRE_MED'] ? prescriptionItem['QT_ITPRE_MED'] : null,
                unit: prescriptionItem['DS_UNIDADE'] ? prescriptionItem['DS_UNIDADE'] : null,
                comments: prescriptionItem['DS_ITPRE_MED'] ? prescriptionItem['DS_ITPRE_MED'] : null,
                application_method: prescriptionItem['DS_FOR_APL'] ? prescriptionItem['DS_FOR_APL'] : null,
                frequency_id: prescriptionItem['CD_TIP_FRE'] ? frequencyTable.find(frequency => frequency.external_id == prescriptionItem['CD_TIP_FRE']).id : null,
                healthcare_professional_id: prescriptionItem['NM_PRESTADOR'] ? professionalTable.find(professional => professional.personal_info.name == prescriptionItem['NM_PRESTADOR']).id : null,
                admission_prescription_id: prescriptionItem['CD_PRE_MED'] ? prescriptionTable.find(admission => admission.external_id == prescriptionItem['CD_PRE_MED']).id : null
            }
            prescriptionItemArray.push(currentPrescriptionItem);
        });
        this.logger.log(`[SyncPrescriptionItemService] ${prescriptionItemArray.length} rows generated for entity 'PrescriptionItem'`);
        return prescriptionItemArray;
    }

    async bulkCreatePrescriptionItemsAux(prescriptionItemsAuxArray: Array<object[]>) {
        try {
            this.logger.log(`[SyncPrescriptionItemService] Inserting ${prescriptionItemsAuxArray[0].length} rows of data into 'prescription_item_frequency' table...`);
            await this.prescriptionItemFrequencyProvider.bulkCreate(prescriptionItemsAuxArray[0], {
                updateOnDuplicate: [
                    'frequency',
                    'value',
                    'fixed',
                    'type'
                ]
            });
            this.logger.log(`[SyncPrescriptionItemService] Inserting ${prescriptionItemsAuxArray[2].length} rows of data into 'prescription_item_description' table...`);
            await this.prescriptionItemDescriptionProvider.bulkCreate(prescriptionItemsAuxArray[2], {
                updateOnDuplicate: [
                    'description'
                ]
            });
            this.logger.log(`[SyncPrescriptionItemService] Inserting ${prescriptionItemsAuxArray[1].length} rows of data into 'prescription_item_group' table...`);
            await this.prescriptionItemGroupProvider.bulkCreate(prescriptionItemsAuxArray[1], {
                updateOnDuplicate: [
                    'name'
                ]
            });
        } catch (error) {
            this.logger.log(`[SyncPrescriptionItemService] An error occured: ${error}`);
        }
    }

    async bulkCreatePrescriptionItems(prescriptionItemsArray: Array<{}>) {
        try {
            this.logger.log(`[SyncPrescriptionItemService] Inserting ${prescriptionItemsArray.length} rows of data into 'prescription_item' table...`);
            await this.prescriptionItemProvider.bulkCreate(prescriptionItemsArray, {
                updateOnDuplicate: [
                    'quantity',
                    'unit',
                    'comments',
                    'application_method'
                ]
            });
        } catch (error) {
            this.logger.log(`[SyncPrescriptionItemService] An error occured: ${error}`);
        }
    }

}

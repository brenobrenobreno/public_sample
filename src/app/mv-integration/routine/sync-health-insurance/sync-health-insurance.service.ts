import { Injectable, Inject } from '@nestjs/common';

import { HealthInsurance } from './../../../health-insurance/health-insurance.entity';

import { LoggerService } from './../../../logger/logger.service';
import { UtilsService } from '../../utils/utils.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncHealthInsuranceService {
    constructor(
        @Inject('HEALTH_INSURANCE_REPOSITORY') private healthInsuranceProvider: typeof HealthInsurance,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncHealthInsuranceService] Starting HealthInsurance data sync`);

        const MV_result = await this.utils.getDatafromAPI('/convenios');
        const healthInsuranceFormattedData = await this.formatHealthInsurances(MV_result);
        await this.bulkCreateHealthInsurances(healthInsuranceFormattedData);

        this.logger.log(`[SyncHealthInsuranceService] HealthInsurance data sync complete`);
    }

    async formatHealthInsurances(healthInsurancesFromMV: Array<{}>) {
        this.logger.log(`[SyncHealthInsuranceService] Formatting ${healthInsurancesFromMV.length} data objects...`);

        let healthInsuranceArray: Array<{}> = [];

        healthInsurancesFromMV.forEach((healthInsurance) => {
            const healthInsuranceId = uuidv4();
            const currentHealthInsurance = {
                id: healthInsuranceId,
                external_id: healthInsurance['CD_CONVENIO_PRIMARIO'] ? healthInsurance['CD_CONVENIO_PRIMARIO'] : null,
                name: healthInsurance['NM_CONVENIO_PRIMARIO'] ? healthInsurance['NM_CONVENIO_PRIMARIO'] : null,
                plan: healthInsurance['DS_PLANO_CONVENIO_PRIMARIO'] ? healthInsurance['DS_PLANO_CONVENIO_PRIMARIO'] : null
            };
            healthInsuranceArray.push(currentHealthInsurance);
        })
        this.logger.log(`[SyncHealthInsuranceService] ${healthInsuranceArray.length} rows generated for entity 'HealthInsurance'`);
        return healthInsuranceArray;
    }

    async bulkCreateHealthInsurances(healthInsuranceArray: Array<{}>) {
        this.logger.log(`[SyncHealthInsuranceService] Inserting ${healthInsuranceArray.length} rows of data into 'health_insurance' table...`);
        await this.healthInsuranceProvider.bulkCreate(healthInsuranceArray, {
            updateOnDuplicate: [
                'name',
                'plan'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncHealthInsuranceService] An error occured: ${error}`);
        });
    }
}

import { Injectable, Inject } from '@nestjs/common';

import { ProfessionalBoard } from './../../../backoffice/professional-board/professional-board.entity';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncProfessionalBoardService {
    constructor(
        @Inject('PROFESSIONAL_BOARD_REPOSITORY') private professionalBoardProvider: typeof ProfessionalBoard,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncProfessionalBoardService] Starting ProfessionalBoard data sync`);
        
        const MV_result = await this.utils.getDatafromAPI('/prestadores');
        const formattedData = this.formatBulkProfessionalBoard(MV_result);
        await this.bulkCreateProfessionalBoard(formattedData);

        this.logger.log(`[SyncProfessionalBoardService] ProfessionalBoard data sync complete`);
    }

    formatBulkProfessionalBoard(prescriptionsFromMV: Array<{}>) {
        this.logger.log(`[SyncProfessionalBoardService] Formatting ${prescriptionsFromMV.length} data objects...`);

        let boardArray: Array<{}> = [];

        prescriptionsFromMV.forEach((prescription) => {
            const currentProfessionalBoard = {
                id: uuidv4(),
                external_id: prescription['COD_CONSELHO'] ? prescription['COD_CONSELHO'] : null,
                name: prescription['CONSELHO_PRESTADOR'] ? prescription['CONSELHO_PRESTADOR'] : null,
                region: prescription['UF_CONSELHO_PRESTADOR'] ? prescription['UF_CONSELHO_PRESTADOR'] : null,
            };
            boardArray.push(currentProfessionalBoard);
        });
        this.logger.log(`[SyncProfessionalBoardService] ${boardArray.length} rows generated for entity 'ProfessionalBoard'`);
        return boardArray;
    }

    async bulkCreateProfessionalBoard(boardArray: Array<{}>) {
        this.logger.log(`[SyncProfessionalBoardService] Inserting ${boardArray.length} rows of data into 'professional_board' table...`);
        await this.professionalBoardProvider.bulkCreate(boardArray, {
            updateOnDuplicate: [
                'name',
                'region'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncProfessionalBoardService] An error occured: ${error}`);
        });
    }

}

import { Injectable, Inject } from '@nestjs/common';

import { ContactInfo } from './../../../contact-info/contact-info.entity';
import { Patient } from './../../../patient/patient.entity';
import { Person } from './../../../person/person.entity';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from './../../../logger/logger.service';

import { MaritalStatus } from './../../../shared/enum/marital-status';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncPatientService {
    constructor(
        @Inject('PERSONS_REPOSITORY') private personsProvider: typeof Person,
        @Inject('PATIENTS_REPOSITORY') private patientsProvider: typeof Patient,
        @Inject('CONTACT_INFO_REPOSITORY') private contactInfoProvider: typeof ContactInfo,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncPatientService] Starting Patient data sync`);

        const MV_result = await this.utils.getDatafromAPI('/pacientes-internados');
        const formattedData = this.formatBulkPatientToBulkPerson(MV_result);
        await this.bulkCreatePatient(formattedData);

        this.logger.log(`[SyncPatientService] Patient data sync complete`);
    }

    formatBulkPatientToBulkPerson(patientsFromMV: Array<{}>) {
        this.logger.log(`[SyncPatientService] Formatting ${patientsFromMV.length} data objects...`);

        let contactInfoDtoArray: Array<{}> = [];
        let personDtoArray: Array<{}> = [];
        let patientDtoArray: Array<{}> = [];

        patientsFromMV.forEach((patient) => {
            const contactInfoId = uuidv4();
            const personId = uuidv4();
            const patientId = uuidv4();

            const currentContactInfo = {
                id: contactInfoId,
                external_id: null,
                email: patient['EMAIL'] ? patient['EMAIL'] : null,
                landline_number: patient['NR_DDD_FONE'] ? patient['NR_DDD_FONE'].toString() + patient['NR_FONE'] : patient['NR_FONE'],
                cellphone_number: patient['NR_DDD_CELULAR'] ? patient['NR_DDD_CELULAR'].toString() + patient['NR_CELULAR'] : patient['NR_CELULAR'],
                social_media_chat: null
            };

            const currentPerson = {
                id: personId,
                external_id: null,
                photo_id_code: null,
                name: patient['NM_PACIENTE'] ? patient['NM_PACIENTE'] : '',
                social_name: null,
                gender: patient['TP_SEXO'] ? patient['TP_SEXO'] : '',
                birthday: patient['DT_NASCIMENTO'] ? this.utils.formatDate(patient['DT_NASCIMENTO']) : '',
                marital_status: patient['TP_ESTADO_CIVIL'] ? this.utils.formatMaritalStatus(patient['TP_ESTADO_CIVIL']) : MaritalStatus.unknown,
                social_security_number: patient['NR_IDENTIDADE'] ? patient['NR_IDENTIDADE'] : null,
                alternative_social_security_number: patient['NR_CPF'] ? patient['NR_CPF'] : null,
                zip_code: patient['NR_CEP'] ? patient['NR_CEP'] : null,
                street: patient['DS_ENDERECO'] ? patient['DS_ENDERECO'] : null,
                district: patient['GRACAS'] ? patient['GRACAS'] : null,
                city: patient['NM_CIDADE'] ? patient['NM_CIDADE'] : null,
                state: null,
                address_complement: patient['DS_COMPLEMENTO'] ? patient['DS_COMPLEMENTO'] : null,
                contact_info_id: contactInfoId
            };

            const currentPatientInfo = {
                id: patientId,
                external_id: patient['CD_PACIENTE'] ? patient['CD_PACIENTE'] : null,
                affiliation: this.utils.formatAffiliation(patient['NM_MAE'], patient['NM_PAI']),
                blood_type: patient['TP_SANGUINEO'] ? patient['TP_SANGUINEO'] : null,
                civilly_responsible: false,
                person_id: personId
            };

            personDtoArray.push(currentPerson);
            patientDtoArray.push(currentPatientInfo);
            contactInfoDtoArray.push(currentContactInfo);
        })
        this.logger.log(`[SyncPatientService] ${personDtoArray.length} rows generated for entity 'Person'`);
        this.logger.log(`[SyncPatientService] ${patientDtoArray.length} rows generated for entity 'Patient'`);
        this.logger.log(`[SyncPatientService] ${contactInfoDtoArray.length} rows generated for entity 'ContactInfo'`);
        return [contactInfoDtoArray, personDtoArray, patientDtoArray];
    }

    async bulkCreatePatient(patientArray: Array<object[]>) {
        try {
            this.logger.log(`[SyncPatientService] Inserting ${patientArray[0].length} rows of data into 'contact_info' table...`);
            await this.contactInfoProvider.bulkCreate(patientArray[0], {
                updateOnDuplicate: [
                    'email',
                    'landline_number',
                    'cellphone_number',
                    'social_media_chat'
                ]
            });
            this.logger.log(`[SyncPatientService] Inserting ${patientArray[1].length} rows of data into 'person' table...`);
            await this.personsProvider.bulkCreate(patientArray[1], {
                updateOnDuplicate: [
                    'name',
                    'social_name',
                    'gender',
                    'birthday',
                    'marital_status',
                    'social_security_number',
                    'alternative_social_security_number',
                    'zip_code',
                    'street',
                    'district',
                    'city',
                    'state',
                    'address_complement'
                ]
            });
            this.logger.log(`[SyncPatientService] Inserting ${patientArray[1].length} rows of data into 'patient' table...`);
            await this.patientsProvider.bulkCreate(patientArray[2], {
                updateOnDuplicate: [
                    'affiliation',
                    'blood_type',
                    'civilly_responsible'
                ]
            });
        } catch (error) {
            this.logger.log(`[SyncPatientService] An error occured: ${error}`);
        }
    }
}

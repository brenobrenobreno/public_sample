import { AdmissionPrescriptionZone } from '../../../backoffice/admission-prescription-zone/admission-prescription-zone.entity';
import { Injectable, Inject } from '@nestjs/common';

import { UtilsService } from '../../utils/utils.service';
import { LoggerService } from '../../../logger/logger.service';

import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class SyncAdmissionPrescriptionZoneService {
    constructor(
        @Inject('ADMISSION_PRESCRIPTION_ZONE_REPOSITORY') private admissionPrescriptionZoneProvider: typeof AdmissionPrescriptionZone,
        private readonly utils: UtilsService,
        private readonly logger: LoggerService
    ) {}

    async syncData() {
        this.logger.log(`[SyncAdmissionPrescriptionZoneService] Starting AdmissionPrescriptionZone data sync`);
        
        const MV_result = await this.utils.getDatafromAPI('/prescricoes');
        const formattedData = this.formatBulkAdmissionPrescriptionZone(MV_result);
        await this.bulkCreateAdmissionPrescriptionZone(formattedData);

        this.logger.log(`[SyncAdmissionPrescriptionZoneService] AdmissionPrescriptionZone data sync complete`);
    }

    formatBulkAdmissionPrescriptionZone(prescriptionsFromMV: Array<{}>) {
        this.logger.log(`[SyncAdmissionPrescriptionZoneService] Formatting ${prescriptionsFromMV.length} data objects...`);

        let zoneArray: Array<{}> = [];

        prescriptionsFromMV.forEach((prescription) => {
            const currentZone = {
                id: uuidv4(),
                external_id: prescription['CD_UNID_INT'] ? prescription['CD_UNID_INT'] : null,
                name: prescription['DS_UNID_INT'] ? prescription['DS_UNID_INT'] : '',
            };
            zoneArray.push(currentZone);
        });
        this.logger.log(`[SyncAdmissionPrescriptionZoneService] ${zoneArray.length} rows generated for entity 'AdmissionPrescriptionZone'`);
        return zoneArray;
    }

    async bulkCreateAdmissionPrescriptionZone(zoneArray: Array<{}>) {
        this.logger.log(`[SyncAdmissionPrescriptionZoneService] Inserting ${zoneArray.length} rows of data into 'admission_prescription_zone' table...`);
        await this.admissionPrescriptionZoneProvider.bulkCreate(zoneArray, {
            updateOnDuplicate: [
                'name'
            ]
        }).catch((error) => {
            this.logger.log(`[SyncAdmissionPrescriptionZoneService] An error occured: ${error}`);
        });
    }
}

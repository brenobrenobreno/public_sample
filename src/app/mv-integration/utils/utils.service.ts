import { Injectable } from '@nestjs/common';

import { LoggerService } from '../../logger/logger.service';

import { MaritalStatus } from '../../shared/enum/marital-status';

import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: process.env.DEV_MV_INTEGRATION_API,
    headers: {}
});

@Injectable()
export class UtilsService {
    constructor(
        private readonly logger: LoggerService
    ) {}

    async getDatafromAPI(endpoint: string) {
        this.logger.log(`[UtilsService] Establishing connection to ${endpoint}...`)
        const result = [];
        let i = 0;
        let nextEndpoint: string = endpoint;
        try {
            do {
                let MV_data = await axiosInstance.get(nextEndpoint);
                nextEndpoint = MV_data.data.endpoint;

                MV_data.data.data.forEach(element => {
                    result.push(element);
                })
                this.logger.log(`[UtilsService] Retrieving data from ${endpoint} - pagination #${i++}`)
            } while (nextEndpoint != undefined)
        } catch (error) {
            this.logger.log(`[UtilsService] An error occured: ${error}`);
        }
        this.logger.log(`[UtilsService] Retrieved ${result.length} rows of data from ${endpoint}`)
        return result;
    }

    formatAffiliation(motherName: string, fatherName: string) {
        if (motherName == null) {
            if (fatherName == null) {
                return '';
            } else {
                return fatherName;
            }
        } else {
            return motherName;
        }
    }

    formatFrequencyFix(fix: any) {
        switch(fix) {
            case "S":
                return true;
            case "N":
                return false;
        }
    }

    formatDate(date: string): string {
        return date.split("/").reverse().join("-");
    }

    formatDatetimeToDate(date: string): string {
        return date.split("T")[0];
    }

    formatMaritalStatus(maritalStatus: string): MaritalStatus {
        switch (maritalStatus) {
            case 'C':
                return MaritalStatus.married
            case 'S':
                return MaritalStatus.unmarried
            default:
                return MaritalStatus.unknown
        }
    }

    formatPrescriptionCancelledStatus(status: string): number {
        switch (status) {
            case 'N':
                return 0
            case 'S':
                return 1
            default:
                return 0
        }
    }
}

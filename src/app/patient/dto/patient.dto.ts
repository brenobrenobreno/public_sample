import { BloodTypes } from '../../shared/enum/blood-types';

export class PatientDto {
    id: string;
    external_id: string;
    affiliation: string;
    blood_type: BloodTypes;
    civilly_responsible: boolean;
    person_id: string;
}
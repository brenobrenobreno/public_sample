import { ContactInfo } from './../contact-info/contact-info.entity';
import { Patient } from './patient.entity';
import { Person } from './../person/person.entity';
import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { PatientService } from './patient.service';

@Controller('patients')
export class PatientController {
    constructor(private readonly patientService: PatientService) {}

    @Post()
    create(@Body() patientObjects: [Person, Patient, ContactInfo]): Promise<void> {
      return this.patientService.create(patientObjects);
    }

    @Put()
    update(@Body() patientObjects: [Person, Patient, ContactInfo]): Promise<void> {
      return this.patientService.update(patientObjects);
    }

    @Get()
    findAll(): Promise<Patient[]> {
      return this.patientService.findAll();
    }

    @Get(':patientId')
    findOne(@Param('patientId') patientId: string): Promise<Patient> {
      return this.patientService.findOne(patientId);
    }

    @Delete(':patientId')
    remove(@Param('patientId') patientId: string): Promise<void> {
      return this.patientService.remove(patientId);
    }
}

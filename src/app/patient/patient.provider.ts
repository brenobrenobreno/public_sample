import { Patient } from './patient.entity';

export const patientsProvider = [
    {
      provide: 'PATIENTS_REPOSITORY',
      useValue: Patient,
    },
  ];
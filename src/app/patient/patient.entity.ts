import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, ForeignKey, IsUUID, AllowNull, BelongsTo, Default } from 'sequelize-typescript';
import { BloodTypes } from '../shared/enum/blood-types';
import { Person } from './../person/person.entity';

@Table({
    tableName: 'patient',
})
export class Patient extends Model<Patient> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull
    @Column
    affiliation: string;

    @AllowNull
    @Column({ type:
        DataType.ENUM(
            BloodTypes.aPositive,
            BloodTypes.aNegative,
            BloodTypes.bPositive,
            BloodTypes.bNegative,
            BloodTypes.abPositive,
            BloodTypes.abNegative,
            BloodTypes.oPositive,
            BloodTypes.oNegative)
        })
    blood_type: BloodTypes;

    @AllowNull
    @Column
    civilly_responsible: boolean;

    @ForeignKey(() => Person)
    @Column
    person_id: string;

    @BelongsTo(() => Person)
    personal_info: Person;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

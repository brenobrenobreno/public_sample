import { Module } from '@nestjs/common';

import { PatientService } from './patient.service';
import { PatientController } from './patient.controller';
import { DatabaseModule } from '../../database/database.module';

import { patientsProvider } from './patient.provider';
import { contactInfoProvider } from './../contact-info/contact-info.provider';
import { personsProvider } from './../person/person.provider';

@Module({
  imports: [DatabaseModule],
  exports: [PatientService, ...patientsProvider, ...contactInfoProvider, ...personsProvider],
  providers: [PatientService, ...patientsProvider, ...contactInfoProvider, ...personsProvider],
  controllers: [PatientController]
})
export class PatientModule {}

import { Injectable, Inject } from '@nestjs/common';

import { Person } from './../person/person.entity';
import { Patient } from './patient.entity';
import { ContactInfo } from './../contact-info/contact-info.entity';

@Injectable()
export class PatientService {
    constructor(
        @Inject('PATIENTS_REPOSITORY') private patientProvider: typeof Patient,
        @Inject('PERSONS_REPOSITORY') private personsProvider: typeof Person
    ) {}

    create(patientObjects: [Person, Patient, ContactInfo]): Promise<void> {
        return null;
    }

    async update(patient: {}): Promise<void> {
        return null;
    }

    async findAll(): Promise<Patient[]> {
        const patients = await this.patientProvider.findAll(
            {
                include: [{
                    model: Person,
                    as: 'personal_info',
                    attributes :[
                        'id',
                        'photo_id_code',
                        'name',
                        'social_name',
                        'gender',
                        'birthday',
                        'marital_status',
                        'social_security_number',
                        'alternative_social_security_number',
                        'zip_code',
                        'street',
                        'district',
                        'city',
                        'state',
                        'address_complement'
                    ],
                    include: [{
                        model: ContactInfo,
                        as: 'contact_info',
                        attributes: ['id', 'email', 'landline_number', 'cellphone_number', 'social_media_chat']
                    }]
                }]
            }
        );
        return patients;
    }

    async findOne(patient_id: string): Promise<Patient> {
        const patient = await this.patientProvider.findOne(
            {
                where: {
                    id: patient_id
                },
                include: [{
                    model: Person,
                    as: 'personal_info',
                    attributes :[
                        'id',
                        'photo_id_code',
                        'name',
                        'social_name',
                        'gender',
                        'birthday',
                        'marital_status',
                        'social_security_number',
                        'alternative_social_security_number',
                        'zip_code',
                        'street',
                        'district',
                        'city',
                        'state',
                        'address_complement'
                    ],
                    include: [{
                        model: ContactInfo,
                        as: 'contact_info',
                        attributes: ['id', 'email', 'landline_number', 'cellphone_number', 'social_media_chat']
                    }]
                }]
            }
        );
        return patient;
    }

    async remove(patientId: string): Promise<void> {
        const patient = await this.patientProvider.findOne({ where: { id: patientId } });
        await patient.destroy();
    }
}

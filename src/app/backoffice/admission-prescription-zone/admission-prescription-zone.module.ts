import { Module } from '@nestjs/common';
import { AdmissionPrescriptionZoneService } from './admission-prescription-zone.service';
import { AdmissionPrescriptionZoneController } from './admission-prescription-zone.controller';
import { admissionPrescriptionZoneProvider } from './admission-prescription-zone.provider';

@Module({
  exports: [AdmissionPrescriptionZoneService, ...admissionPrescriptionZoneProvider],
  providers: [AdmissionPrescriptionZoneService, ...admissionPrescriptionZoneProvider],
  controllers: [AdmissionPrescriptionZoneController]
})
export class AdmissionPrescriptionZoneModule {}

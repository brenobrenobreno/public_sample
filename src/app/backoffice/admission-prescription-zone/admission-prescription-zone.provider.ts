import { AdmissionPrescriptionZone } from './admission-prescription-zone.entity';

export const admissionPrescriptionZoneProvider = [
    {
      provide: 'ADMISSION_PRESCRIPTION_ZONE_REPOSITORY',
      useValue: AdmissionPrescriptionZone,
    },
  ];

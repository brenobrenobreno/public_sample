import { AdmissionPrescription } from './../../admission-prescription/admission-prescription.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, NotNull, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { ProfessionalType } from '../professional-type/professional-type.entity';
import { Person } from '../../person/person.entity';

@Table({
    tableName: 'healthcare_professional',
})
export class HealthcareProfessional extends Model<HealthcareProfessional> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @ForeignKey(() => ProfessionalType)
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    professional_type_id: string;

    @AllowNull
    @Column
    board_code: string;

    @ForeignKey(() => Person)
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    person_id: string;

    @BelongsTo(() => ProfessionalType)
    professional_type: ProfessionalType;

    @BelongsTo(() => Person)
    personal_info: Person;

    @HasMany(() => AdmissionPrescription, 'prescription_provider_id')
    provider_prescriptions: AdmissionPrescription[];

    @HasMany(() => AdmissionPrescription, 'prescription_transcriber_id')
    transcriptor_prescriptions: AdmissionPrescription[];

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

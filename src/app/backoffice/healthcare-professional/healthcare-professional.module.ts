import { Module } from '@nestjs/common';
import { HealthcareProfessionalController } from './healthcare-professional.controller';
import { healthcareProfessionalProvider } from './healthcare-professional.provider';
import { HealthcareProfessionalService } from './healthcare-professional.service';

@Module({
  exports:  [HealthcareProfessionalService, ...healthcareProfessionalProvider],
  controllers: [HealthcareProfessionalController],
  providers: [HealthcareProfessionalService, ...healthcareProfessionalProvider]
})
export class HealthcareProfessionalModule {}

import { HealthcareProfessional } from './healthcare-professional.entity';

export const healthcareProfessionalProvider = [
    {
      provide: 'HEALTHCARE_PROFESSIONAL_REPOSITORY',
      useValue: HealthcareProfessional,
    },
  ];
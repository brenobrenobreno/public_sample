import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, NotNull } from 'sequelize-typescript';

@Table({
    tableName: 'patient_supporter_type',
})
export class PatientSupporterType extends Model<PatientSupporterType> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @NotNull
    @Column
    name: string;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

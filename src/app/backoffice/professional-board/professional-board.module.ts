import { Module } from '@nestjs/common';
import { ProfessionalBoardService } from './professional-board.service';
import { ProfessionalBoardController } from './professional-board.controller';
import { professionalBoardProvider } from './professional-board.provider';

@Module({
  exports: [ProfessionalBoardService, ...professionalBoardProvider],
  providers: [ProfessionalBoardService, ...professionalBoardProvider],
  controllers: [ProfessionalBoardController]
})
export class ProfessionalBoardModule {}

import { ProfessionalBoard } from './professional-board.entity';

export const professionalBoardProvider = [
    {
      provide: 'PROFESSIONAL_BOARD_REPOSITORY',
      useValue: ProfessionalBoard,
    },
  ];
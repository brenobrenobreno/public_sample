import { ProfessionalType } from './../professional-type/professional-type.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, NotNull, HasMany } from 'sequelize-typescript';

@Table({
    tableName: 'professional_board',
})
export class ProfessionalBoard extends Model<ProfessionalBoard> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull(false)
    @Column
    name: string;

    @AllowNull(false)
    @Column
    region: string;

    @HasMany(() => ProfessionalType, 'professional_board_id')
    professional_type: ProfessionalType

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

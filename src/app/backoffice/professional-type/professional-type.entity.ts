import { HealthcareProfessional } from './../healthcare-professional/healthcare-professional.entity';
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { ProfessionalBoard } from '../professional-board/professional-board.entity';

@Table({
    tableName: 'professional_type',
})
export class ProfessionalType extends Model<ProfessionalType> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull(false)
    @Column
    name: string;

    @ForeignKey(() => ProfessionalBoard)
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    professional_board_id: string;

    @BelongsTo(() => ProfessionalBoard)
    professional_board: ProfessionalBoard;

    @HasMany(() => HealthcareProfessional, 'professional_type_id')
    healthcare_professional: HealthcareProfessional

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}

import { ProfessionalType } from './professional-type.entity';

export const professionalTypeProvider = [
    {
      provide: 'PROFESSIONAL_TYPE_REPOSITORY',
      useValue: ProfessionalType,
    },
  ];
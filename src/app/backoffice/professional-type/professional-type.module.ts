import { Module } from '@nestjs/common';
import { ProfessionalTypeController } from './professional-type.controller';
import { professionalTypeProvider } from './professional-type.provider';
import { ProfessionalTypeService } from './professional-type.service';

@Module({
  exports: [ProfessionalTypeService, ...professionalTypeProvider],
  controllers: [ProfessionalTypeController],
  providers: [ProfessionalTypeService, ...professionalTypeProvider]
})
export class ProfessionalTypeModule {}

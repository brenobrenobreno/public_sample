import { cidProvider } from './cid.provider';
import { DatabaseModule } from './../../database/database.module';
import { Module } from '@nestjs/common';
import { CidService } from './cid.service';
import { CidController } from './cid.controller';

@Module({
  imports: [DatabaseModule],
  exports: [...cidProvider],
  providers: [CidService, ...cidProvider],
  controllers: [CidController]
})
export class CidModule {}

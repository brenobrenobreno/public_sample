import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull } from 'sequelize-typescript';

@Table({
    tableName: 'cid',
})
export class Cid extends Model<Cid> {
    @PrimaryKey
    @IsUUID(4)
    @Column(DataType.UUIDV4)
    id: string;
  
    @AllowNull
    @Unique
    @Column
    external_id: string;

    @Unique
    @Column
    cid_code: string;

    @Column(DataType.TEXT)
    cid_description: string;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;
  
    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}
import { Cid } from './cid.entity';

export const cidProvider = [
    {
      provide: 'CID_REPOSITORY',
      useValue: Cid,
    },
  ];
export class ContactInfoDto {
    id: string;
    external_id: string;
    email: string;
    landline_number: string;
    cellphone_number: string;
    social_media_chat: boolean;
    person_id: string;
}
import { Module } from '@nestjs/common';
import { ContactInfoService } from './contact-info.service';
import { ContactInfoController } from './contact-info.controller';
import { contactInfoProvider } from './contact-info.provider';
import { DatabaseModule } from '../../database/database.module';

@Module({
  imports: [DatabaseModule],
  exports: [ContactInfoService, ...contactInfoProvider],
  providers: [ContactInfoService, ...contactInfoProvider],
  controllers: [ContactInfoController]
})
export class ContactInfoModule {}

import { ContactInfo } from './contact-info.entity';

export const contactInfoProvider = [
    {
      provide: 'CONTACT_INFO_REPOSITORY',
      useValue: ContactInfo,
    },
  ];
import { Column, Model, Table, CreatedAt, UpdatedAt, PrimaryKey, DataType, Unique, IsUUID, AllowNull, Default, HasOne } from 'sequelize-typescript';
import { Person } from './../person/person.entity';

@Table({
    tableName: 'contact_info',
})
export class ContactInfo extends Model<ContactInfo> {
    @PrimaryKey
    @IsUUID(4)
    @Default(DataType.UUIDV4)
    @Column(DataType.UUIDV4)
    id: string;

    @AllowNull
    @Unique
    @Column
    external_id: string;

    @AllowNull
    @Column
    email: string;

    @AllowNull
    @Column
    landline_number: string;

    @AllowNull
    @Column
    cellphone_number: string;

    @AllowNull
    @Column
    social_media_chat: boolean;

    @HasOne(() => Person)
    person: Person;

    @CreatedAt
    @Column({ field: 'created_at' })
    createdAt: Date;

    @UpdatedAt
    @Column({ field: 'updated_at' })
    updatedAt: Date;
}


import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ContactInfoDto } from './dto/contact-info.dto';
import { ContactInfo } from './contact-info.entity';
import { ContactInfoService } from './contact-info.service';

@Controller('contact-info')
export class ContactInfoController {
    constructor(private readonly contactInfoService: ContactInfoService) {}

    /*
    @Post()
    create(@Body() contactInfoDto: ContactInfoDto): Promise<ContactInfo> {
      return this.contactInfoService.create(contactInfoDto);
    }

    @Get()
    findAll(): Promise<ContactInfo[]> {
      return this.contactInfoService.findAll();
    }
  
    @Get(':id')
    findOne(@Param('id') id: string): Promise<ContactInfo> {
      return this.contactInfoService.findOne(id);
    }
  
    @Delete(':id')
    remove(@Param('id') id: string): Promise<void> {
      return this.contactInfoService.remove(id);
    }
    */

}

import { Injectable, Inject } from '@nestjs/common';
import { ContactInfo } from './contact-info.entity';
import { ContactInfoDto } from './dto/contact-info.dto';

@Injectable()
export class ContactInfoService {

    constructor(
        @Inject('CONTACT_INFO_REPOSITORY') private contactInfoRepository: typeof ContactInfo) {}

    async findAll(): Promise<ContactInfo[]> {
        return this.contactInfoRepository.findAll();
    }

    create(contactInfoDto: ContactInfoDto): Promise<ContactInfo> {
        return null;
    }

    findOne(id: string): Promise<ContactInfo> {
        return this.contactInfoRepository.findOne({
          where: {
            id,
          },
        });
    }

    async remove(id: string): Promise<void> {
        const contactInfo = await this.findOne(id);
        await contactInfo.destroy();
    }


}

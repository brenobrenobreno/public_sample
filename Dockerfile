FROM node:12.14.0-alpine3.11

RUN apk add --no-cache bash

COPY . neri-api

WORKDIR /neri-api

RUN npm install -g @nestjs/cli rimraf sequelize serverless @angular/cli;

COPY . .

RUN npm install
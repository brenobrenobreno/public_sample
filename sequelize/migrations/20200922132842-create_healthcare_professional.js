'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('healthcare_professional', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      professional_type_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'professional_type',
          key: 'id'
        }
      },
      board_code: {
        allowNull: true,
        type: Sequelize.STRING
      },
      person_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'person',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('healthcare_professional');
  }
};

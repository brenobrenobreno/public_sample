'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('admission_prescription', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      admission_prescription_cancelled: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      initial_datetime: {
        allowNull: false,
        type: Sequelize.DATE
      },
      final_datetime: {
        allowNull: false,
        type: Sequelize.DATEONLY
      },
      admission_prescription_zone_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'admission_prescription_zone',
          key: 'id'
        }
      },
      patient_admission_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'patient_admission',
          key: 'id'
        }
      },
      prescription_provider_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'healthcare_professional',
          key: 'id'
        }
      },
      prescription_transcriber_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'healthcare_professional',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('admission_prescription');
  }
};

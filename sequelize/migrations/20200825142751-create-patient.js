'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('patient', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      affiliation: {
        allowNull: false,
        type: Sequelize.STRING
      },
      blood_type: {
        allowNull: true,
        type: Sequelize.ENUM,
        values: ['A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-']
      },
      civilly_responsible: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      person_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'person',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('patient');
  }
};

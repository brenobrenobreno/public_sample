'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('admission_cid', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      cid_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'cid',
          key: 'id'
        }
      },
      admission_cid_state: {
        allowNull: false,
        type: Sequelize.ENUM,
        values: ['ACTIVE', 'INACTIVE'],
        defaultValue: 'ACTIVE'
      },
      patient_admission_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'patient_admission',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('admission_cid');
  }
};

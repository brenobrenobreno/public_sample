'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('patient_supporter', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      supporter_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      supporter_social_security_number: {
        allowNull: true,
        type: Sequelize.STRING
      },
      supporter_alternative_social_security_number: {
        allowNull: true,
        type: Sequelize.STRING
      },
      phone_number: {
        allowNull: true,
        type: Sequelize.STRING
      },
      patient_supporter_type_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'patient_supporter_type',
          key: 'id'
        }
      },
      patient_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'patient',
          key: 'id'
        }
      },
      contact_info_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'contact_info',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('patient_supporter');
  }
};

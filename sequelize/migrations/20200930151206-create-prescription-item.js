'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('prescription_item', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      prescription_item_group_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'prescription_item_group',
          key: 'id'
        }
      },
      prescription_item_description_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'prescription_item_description',
          key: 'id'
        }
      },
      quantity: {
        allowNull: true,
        type: Sequelize.DOUBLE
      },
      unit: {
        allowNull: true,
        type: Sequelize.STRING
      },
      comments: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      application_method: {
        allowNull: true,
        type: Sequelize.STRING
      },
      frequency_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'prescription_item_frequency',
          key: 'id'
        }
      },
      healthcare_professional_id: {                                                         
        allowNull: true,
        type: Sequelize.UUID,
        references: {
          model: 'healthcare_professional',
          key: 'id'
        }
      },
      admission_prescription_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'admission_prescription',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('prescription_item');
  }
};

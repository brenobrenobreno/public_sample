'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('admission_health_insurance', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      health_insurance_holder: {
        allowNull: true,
        type: Sequelize.STRING
      },
      health_insurance_validity: {
        allowNull: true,
        type: Sequelize.DATE
      },
      health_insurance_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'health_insurance',
          key: 'id'
        }
      },
      patient_health_insurance_register: {
        allowNull: true,
        type: Sequelize.STRING
      },
      patient_admission_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'patient_admission',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('admission_health_insurance');
  }
};

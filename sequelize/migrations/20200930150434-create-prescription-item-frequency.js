'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('prescription_item_frequency', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      frequency: {
        allowNull: false,
        type: Sequelize.STRING
      },
      value: {
        allowNull: true,
        type: Sequelize.FLOAT
      },
      fixed: {
        allowNull: false,
        type: Sequelize.BOOLEAN
      },
      type: {
        allowNull: true,
        type: Sequelize.ENUM,
        defaultValue: 'H',
        values: ['D', 'H', 'M']
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('prescription_item_frequency');
  }
};

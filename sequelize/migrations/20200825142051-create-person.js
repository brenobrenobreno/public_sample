'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('person', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      photo_id_code: {
        allowNull: true,
        type: Sequelize.STRING
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      social_name: {
        allowNull: true,
        type: Sequelize.STRING
      },
      gender: {
        allowNull: false,
        type: Sequelize.ENUM,
        values: ['M', 'F', 'O', 'U']
      },
      birthday: {
        allowNull: false,
        type: Sequelize.DATEONLY
      },
      marital_status: {
        allowNull: true,
        type: Sequelize.ENUM,
        values: ['ANNULLED', 'DIVORCED', 'DOMESTIC_PARTNER', 'INTERLOCUTORY', 'LEGALLY_SEPARATED', 
                 'MARRIED', 'NEVER_MARRIED', 'POLYGAMOUS', 'UNMARRIED', 'WIDOWED', 'UNKNOWN']
      },
      social_security_number: {
        allowNull: true,
        type: Sequelize.STRING
      },
      alternative_social_security_number: {
        allowNull: true,
        type: Sequelize.STRING
      },
      zip_code: {
        allowNull: true,
        type: Sequelize.STRING
      },
      street: {
        allowNull: true,
        type: Sequelize.STRING
      },
      district: {
        allowNull: true,
        type: Sequelize.STRING
      },
      city: {
        allowNull: true,
        type: Sequelize.STRING
      },
      state: {
        allowNull: true,
        type: Sequelize.STRING
      },
      address_complement: {
        allowNull: true,
        type: Sequelize.STRING
      },
      contact_info_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'contact_info',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('person');
  }
};

'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('patient_allergy', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      status: {
        allowNull: true,
        type: Sequelize.STRING
      },
      description: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      severity: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      substance_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'substance',
          key: 'id'
        }
      },
      patient_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'patient',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('substance');
  }
};

'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('prescription_item_component', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      product_id: {                                                            
        allowNull: true,
        type: Sequelize.UUID,
        references: {
          model: 'product',
          key: 'id'
        }
      },
      quantity: {
        allowNull: true,
        type: Sequelize.DOUBLE
      },
      prescription_item_id: {                                                            
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'prescription_item',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('prescription_item_component');
  }
};

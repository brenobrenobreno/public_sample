'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('patient_admission', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      external_id: {
        allowNull: true,
        unique: true,
        type: Sequelize.STRING
      },
      patient_admission_datetime: {
        allowNull: false,
        type: Sequelize.DATE
      },
      patient_admission_state: {
        allowNull: false,
        type: Sequelize.ENUM,
        values: ['ACTIVE', 'INACTIVE'],
        defaultValue: 'ACTIVE'
      },
      palliative_care: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      public_health_insurance_register: {
        allowNull: true,
        type: Sequelize.STRING
      },
      confidential_medical_record: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      long_term: {
        allowNull: true,
        type: Sequelize.BOOLEAN
      },
      patient_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: 'patient',
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('patient_admission');
  }
};

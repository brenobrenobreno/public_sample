# Run With Docker:
[Tutorial](docs/Docker.md)

---

## NestJS + Sequelize + Serverless for the 3i Lean Manager API
### Modules
This API has 2 main modules:
- the Sync module, responsible for retrieving bulk data from the MV database;
- the Patient module, responsible for CRUD operations inside the 3i database.

Check the `serverless.yml` file to see which endpoints are available.

### General dependencies
Create a .env (root path) file with the following configs:
```
DEV_DB_HOST=localhost
DEV_DB_PORT=3306
DEV_DB_USERNAME=<YOUR_USERNAME>
DEV_DB_PASSWORD=<YOUR_PASSWORD>
DEV_DB_SCHEMA=<YOUR_SCHEMA>
DEV_MV_INTEGRATION_API=<MV_INTEGRATION_API_PATH>
```
## Running with serverless offline
#### Dependencies:
```
$ npm install @nestjs/cli serverless -g
$ npm install
```
#### Run:
Run the db:migrate command (at the project root) to create all the required tables.
```
npx sequelize db:migrate
```
Run the ```mv-integration-layer``` API, and have it running on background to access its endpoints. By default, both APIs would run on port ```3000```, so you must add the ```--httpPort``` parameter when running the Serverless-offline instance, to avoid conflicts with the mv-integration API.

Run the following commands:
```
$ npm run build
$ sls offline --httpPort <AVAILABLE_PORT>
```

The ```build``` command will package the NestJS files, and the Serverless instance will run any routes you defined on your controllers starting from ```http://localhost:<AVAILABLE_PORT>/dev/```.

## Running with Docker
#### Software dependencies:
1. [DOCKER](https://docs.docker.com/get-docker/)
#### Run:
```
$ docker-compose up
```
## Endpoints
#### Main endpoint
```GET mv-integration/all``` - Retrieve ALL information
#### Provisioning endpoints (run these before all others)
```GET mv-integration/cid``` - Retrieve CID information (Entities: Cid)

```GET mv-integration/insurances``` - Retrieve health insurance information (entities: HealthInsurance)

```GET mv-integration/substances``` - Retrieve substance information (entities: Substance)

```GET mv-integration/boards``` - Retrieve professional board information (entities: ProfessionalBoard)

```GET mv-integration/units``` - Retrieve admission prescription units information (entities: AdmissionPrescriptionZone)

#### Routine endpoints (depend on entities created from the endpoints above)
```GET mv-integration/patients``` - Retrieve patient information (entities: ContactInfo, Person, Patient)

```GET mv-integration/allergies``` - Retrieve patient allergy information (entities: PatientAllergy)

```GET mv-integration/admission``` - Retrieve patient admission information (entities: PatientAdmission, AdmissionHealthInsurance, AdmissionCid)

```GET mv-integration/types``` - Retrieve professional types information (entities: ProfessionalType)

```GET mv-integration/professionals``` - Retrieve healthcare professional information (entities: ContactInfo, Person, HealthcareProfessional)

```GET mv-integration/prescriptions``` - Retrieve admission prescription information (entities: AdmissionPrescription)

```GET mv-integration/prescription-items``` - Retrieve prescription item information (entities: PrescriptionItemGroup, PrescriptionItemFrequency, PrescriptionItemDescription, PrescriptionItem)

#### Local CRUD endpoints (usable after patient data sync)
#### Patient API
Response structure:
```
[
    patient {
        personal_info {
            contact_info
        }
    }
]
```

```GET patients``` - Retrieve list of all patients

```GET patients/id``` - Retrieve a single patient

#### Admission API
Response structure (note: the ```admission_cid[]``` and ```admission_health_insurance[]``` arrays contain a list of objects of the ```AdmissionCid``` and ```AdmissionHealthInsurance``` types, respectively)
```
[
    admission {
        patient,
        admission_cid [
            admission_cid {
                cid
            }
        ],
        admission_health_insurance [
            admission_health_insurance {
                health_insurance
            }
        ]
    }
]
```
```GET admissions``` - Retrieve list of all patient admissions

```GET admissions/id``` - Retrieve a single patient admission

```GET admissions/patient/patientId``` - Retrieve a list of all admissions related to a single patient

#### Prescription API
Response structure (note: both ```provider``` and ```transcriber``` objects are of the ```HealthcareProfessional``` type)
```
[
    {
        prescription {
            admission_prescription_unit,
            provider {
                personal_info,
                professional_type {
                    professional_board
                }
            },
            transcriber {
                personal_info,
                professional_type {
                    professional_board
                }
            }
        }
    }
]
```
```GET prescriptions``` - Retrieve list of all prescriptions

```GET prescriptions/id``` - Retrieve a single prescription

```GET prescriptions/admission/admissionId``` - Retrieve a list of all prescriptions related to a single admission

#### Prescription Item API
Response structure
```
[
    {
        prescription_item,
        prescription_item_description,
        prescription_item_group,
        prescription_item_frequency,
        healthcare_professional,
        admission_prescription
    }
]
```
```GET prescription-items``` - Retrieve list of all prescription items

```GET prescription-items/id``` - Retrieve a single prescription item

```GET prescription-items/prescription/id``` - Retrieve a list of all prescription items related to a single prescription

#### Prescription Item Component API
Response structure
```
[
    prescription_item_component {
        product
    }
]
```
```GET components``` - Retrieve list of all prescription items components (warning: result size very large)

```GET components/id``` - Retrieve a single prescription item component

```GET components/item/id``` - Retrieve a list of all prescription items components related to a single prescription item
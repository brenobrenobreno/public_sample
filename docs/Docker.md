
# Docker Container - 3i-neri-api
This container is responsible to the application backend and communication with the 3i database.

## Getting Started

These instructions will cover usage information and for the docker container:

#### 1. Clone repository
```shell
git clone https://<YOUR_BITBUCKET_USER>@bitbucket.org/nutes/3i-neri-api.git
```
#### 2. Install Docker:
* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)
#### 3. Create Environment Variables

Create a **.env** file at the main project path with the variables above:

* `DEV_DB_HOST`=DB_HOST
* `DEV_DB_PORT`=DB_PORT
* `DEV_DB_SCHEMA`=DB_SCHEMA
* `DEV_DB_USERNAME`=DB_USER
* `DEV_DB_PASSWORD`=DB_PASSWD
* `DEV_MV_INTEGRATION_API`='DB_URL'

#### 4. Build / Run container.

Build:

```shell
docker build --tag 3i-neri-api:latest
```
Run:
```shell
docker run --publish 3003:3003 --detach --name 3i-neri-api 3i-neri-api:latest
```
#### 5. Access
```shell
http://localhost:3003
```

### Ports
* `3003`

#### Volumes

* `.:/neri-api`

#### Useful File Locations

* `.docker/entrypoint.sh` - Commands needed to run the application

## Basic Docker Commands:
1. List all containers: **$ docker ps -a**
2. Stop one or more running containers: **$ docker stop <container_name>**
3. Remove one or more containers: **$ docker rm -f <container_name>**
4. Open container bash: **$ docker exec -it <container_name> /bin/bash**
5. View logs in real time: **$ docker logs -f <container_name>**
5. List all images: **$ docker images**
6. Remove all containers/images/volumes (Clear all): **$ docker system prune**

## Versioning
## Acknowledgments

* Luck and destiny

---

# Docker-compose

It is also possible to execute the entire project and its all interconnected containers with a single command:
```shelll
docker-compose up -d
```
This commando is valid only this project.